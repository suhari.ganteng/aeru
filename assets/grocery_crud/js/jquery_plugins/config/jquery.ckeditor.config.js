$(function () {
  $("textarea.texteditor").ckeditor({
    toolbar: "Full",
    filebrowserBrowseUrl: baseURL + "explorer/plugins",
    toolbar: [
      ["Image", "Table", "Print"],
      [
        "Bold",
        "Italic",
        "Underline",
        "Strike",
        "Subscript",
        "Superscript",
        "-",
        "RemoveFormat",
      ],
      ["TextColor", "BGColor"],
      ["Maximize", "ShowBlocks"],
    ],
    startupShowBorders: false,
  });
  $("textarea.mini-texteditor").ckeditor({
    toolbar: "Basic",
    width: 700,
    filebrowserBrowseUrl: baseURL + "explorer/plugins",
    toolbar: [
      ["Image", "Table", "Print"],
      [
        "Bold",
        "Italic",
        "Underline",
        "Strike",
        "Subscript",
        "Superscript",
        "-",
        "RemoveFormat",
      ],
      ["TextColor", "BGColor"],
      ["Maximize", "ShowBlocks"],
    ],
    startupShowBorders: false,
  });
});
