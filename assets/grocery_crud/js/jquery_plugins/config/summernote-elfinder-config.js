$(function () {
  var config = {
    toolbar: [
      ["style", ["style"]],
      ["font", ["bold", "underline", "clear"]],
      ["fontname", ["fontname"]],
      ["color", ["color"]],
      ["para", ["ul", "ol", "paragraph"]],
      ['insert', ['image','elfinder']],
      ["table", ["table"]],
      ["view", ["fullscreen", "codeview", "help"]],
    ],
  };
  $("textarea.texteditor").summernote(config);
  $("textarea.mini-texteditor").summernote(config);
});
$('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', 'https://cdnjs.cloudflare.com/ajax/libs/elfinder/2.1.57/css/elfinder.full.min.css') );
$('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', 'https://cdnjs.cloudflare.com/ajax/libs/elfinder/2.1.57/css/theme.min.css') );

$('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') );
$.getScript('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js');
$.getScript('https://cdnjs.cloudflare.com/ajax/libs/elfinder/2.1.57/js/elfinder.min.js');




// jQuery.fn.tooltip = _tooltip; // <--- Then restore it here

(function (factory) {
  /* global define */
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // Node/CommonJS
    module.exports = factory(require('jquery'));
  } else {
    // Browser globals
    factory(window.jQuery);
  }
}(function ($) {
    
  // Extends plugins for adding readmore.
  //  - plugin is external module for customizing.
  $.extend($.summernote.plugins, {
    /**
      * @param {Object} context - context object has status of editor.
      */
     'elfinder': function (context) {
      var self = this;

      // ui has renders to build ui elements.
      //  - you can create a button with `ui.button`
      var ui = $.summernote.ui;

      // add elfinder button
      context.memo('button.elfinder', function () {
          // create button
          var button = ui.button({
              contents: '<i class="fa fa-list-alt"/> Add Picture',
              tooltip: 'elfinder',
              click: function () {
                  elfinderDialog(context);
              }
          });

          // create jQuery object from button instance.
          var $elfinder = button.render();
          return $elfinder;
      });

      // This methods will be called when editor is destroyed by $('..').summernote('destroy');
      // You should remove elements on `initialize`.
      this.destroy = function () {
          this.$panel.remove();
          this.$panel = null;
      };
  }
      
  });
}));

function elfinderDialog(context) {
  var fm = $("<div/>")
    .dialogelfinder({
      url: baseURL + "explorer/connector",
      uiOptions : {
        toolbar : [],
      },
      lang: "en",
      width: 840,
      height: 450,
      destroyOnClose: true,
      getFileCallback: function (file, fm) {
        console.log(file);
        // $('textarea.texteditor').summernote('editor.insertImage', fm.convAbsUrl(file.url)); //...before
        context.invoke("editor.insertImage", fm.convAbsUrl(file.url)); // <------------ after
        // $('textarea.texteditor').summernote('editor.insertImage', file.url);
      },
      commandsOptions: {
        getfile: {
          oncomplete: "close",
          folders: false,
        },
      },
    })
    .dialogelfinder("instance");
}
