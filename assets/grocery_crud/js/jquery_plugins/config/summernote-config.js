$(function () {
  var config = {
    toolbar: [
      ["style", ["style"]],
      ["font", ["bold", "underline", "clear"]],
      ["fontname", ["fontname"]],
      ["color", ["color"]],
      ["para", ["ul", "ol", "paragraph"]],
      ["insert", ["elfinder"]],
      ["table", ["table"]],
      ["view", ["fullscreen", "codeview", "help"]],
    ],
  };
  $("textarea.texteditor").summernote(config);
  $("textarea.mini-texteditor").summernote(config);
});


