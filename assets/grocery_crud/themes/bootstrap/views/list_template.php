<?php
$this->set_css($this->default_theme_path . '/bootstrap/css/common.css');
$this->set_css($this->default_theme_path . '/bootstrap/css/list.css');
$this->set_css($this->default_theme_path . '/bootstrap/css/general.css');
$this->set_css($this->default_theme_path . '/bootstrap/css/plugins/animate.min.css');

$this->set_js_lib($this->default_theme_path . '/bootstrap/js/jquery-plugins/jquery.form.js');
$this->set_js_lib($this->default_theme_path . '/bootstrap/js/common/cache-library.js');
$this->set_js_lib($this->default_theme_path . '/bootstrap/js/common/common.js');
// $this->set_js_lib($this->default_theme_path . '/bootstrap/js/bootstrap/modal.min.js');
$this->set_js_lib($this->default_theme_path . '/bootstrap/js/jquery-plugins/bootstrap-growl.min.js');
$this->set_js_lib($this->default_theme_path . '/bootstrap/js/jquery-plugins/jquery.print-this.js');
$this->set_js_lib($this->default_theme_path . '/bootstrap/js/datagrid/gcrud.datagrid.js');
$this->set_js_lib($this->default_theme_path . '/bootstrap/js/datagrid/list.js');

$this->set_css($this->default_theme_path . '/bootstrap/plugins/stacktable/stacktable.css');
$this->set_js_lib($this->default_theme_path . '/bootstrap/plugins/stacktable/stacktable.js');

$colspans = (count($columns) + 2);

//Start counting the buttons that we have:
$buttons_counter = 0;

if (!$unset_edit) {
    $buttons_counter++;
}

if (!$unset_read) {
    $buttons_counter++;
}

if (!$unset_delete) {
    $buttons_counter++;
}

if (!empty($list[0]) && !empty($list[0]->action_urls)) {
    $buttons_counter = $buttons_counter +  count($list[0]->action_urls);
}

$list_displaying = str_replace(
    array(
        '{start}',
        '{end}',
        '{results}'
    ),
    array(
        '<span class="paging-starts">1</span>',
        '<span class="paging-ends">10</span>',
        '<span class="current-total-results">' . $this->get_total_results() . '</span>'
    ),
    $this->l('list_displaying')
);
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url(); ?>';

    var subject = '<?php echo $subject ?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list_url = '<?php echo $ajax_list_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';

    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
</script>

<style>
    input:focus {
        outline: none;
    }
</style>
<div class=" gc-container">
    <div class="success-message hidden">
        <?php if ($success_message !== null) { ?>
            <?php echo $success_message; ?> &nbsp; &nbsp;
        <?php } ?>
    </div>

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">
                <i class="icon-stack2" style="font-size: 16px;margin-right: 10px;"></i> <?php echo $subject_plural; ?>
            </h3>
            <div class="box-tools pull-right">
                <?php
                if (!empty($this->actions_header)) {
                    echo '<div class="btn-group">';
                    foreach ($this->actions_header as $key => $value) {
                        echo ($value);
                    }
                    echo '</div>';
                }
                ?>
                <a class="btn btn-default btn-sm search-button">
                    <i class="icon-search4"></i>
                    <input type="text" name="search" class="search-input" />
                </a>
                <div class="btn-group">
                    <?php if (!$unset_add) { ?>
                        <a class="btn btn-sm bg-blue" href="<?php echo $add_url ?>">
                            <i class="icon-add" style="margin-right: 10px;"></i>
                            <span class="hidden-xs">
                                <?php echo $this->l('list_add'); ?>
                            </span>
                        </a>
                    <?php } ?>
                    <?php if (!$unset_export) { ?>
                        <a class="btn btn-default btn-sm  gc-export" data-url="<?php echo $export_url; ?>">
                            <i class="icon-file-excel" style="margin-right: 10px;"></i>
                            <span class="hidden-xs">
                                <?php echo $this->l('list_export'); ?>
                            </span>
                        </a>
                    <?php } ?>
                    <?php if (!$unset_print) { ?>
                        <a class="btn btn-default btn-sm  gc-print" data-url="<?php echo $print_url; ?>">
                            <i class="icon-printer" style="margin-right: 10px;"></i>
                            <span class="hidden-xs">
                                <?php echo $this->l('list_print'); ?>
                            </span>
                            <div class="clear"></div>
                        </a>
                    <?php } ?>
                </div>
                <div class="btn-group only-desktops">
                    <a href="javascript:void(0);" class="btn btn-default btn-sm gc-refresh">
                        <i class="icon-reload-alt"></i>
                    </a>
                    <button class="btn btn-default btn-sm minimize-maximize-container minimize-maximize">
                        <i class="fa fa-caret-up"></i>
                    </button>
                    <button class="btn btn-default btn-sm gc-full-width">
                        <i class="fa fa-expand"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="my-scrollbar">
                    <?php echo form_open("", 'method="post" autocomplete="off" id="gcrud-search-form"'); ?>
                    <table class="table grocery-crud-table table-hover table-condensed table-bordered table-striped">
                        <thead>
                            <tr>
                                <?php foreach ($columns as $column) { ?>
                                    <th class="column-with-ordering" data-order-by="<?php echo $column->field_name; ?>"><?php echo $column->display_as; ?></th>
                                <?php } ?>
                                <th rowspan="" <?php if ($buttons_counter === 0) { ?>class="hidden" <?php } ?>>
                                    <?php echo $this->l('list_actions'); ?>
                                </th>
                            </tr>
                            <tr class="filter-row gc-search-row">
                                <?php foreach ($columns as $column) { ?>
                                    <td style="padding: 0;">
                                        <input type="text" style="border-radius:0;border:0;" class="form-control form-control-long input-sm searchable-input floatL" placeholder="Search <?php echo $column->display_as; ?>" name="<?php echo $column->field_name; ?>" />
                                    </td>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php include(__DIR__ . "/list_tbody.php"); ?>
                        </tbody>
                    </table>
                    <?php echo form_close(); ?>

                    <script>
                        $('.grocery-crud-table').cardtable({
                            myClass: 'pagedStackable'
                        });
                        $(document).ajaxStart(function() {
                            // Pace.restart()
                        }).ajaxStop(function() {
                            $(".pagedStackable").remove();
                            $('.grocery-crud-table').removeClass('stacktable large-only')
                            $('.grocery-crud-table').cardtable({
                                myClass: 'pagedStackable'
                            });
                        })
                    </script>
                </div>
            </div>

            <div class="box-footer">
                <div class="footer-tools">
                    <?php if ($total_results > 10) { ?>
                        <div class="floatL  l5">


                            <!-- <div class="input-group">
                        <div class="input-group-addon">
                            <?php list($show_lang_string, $entries_lang_string) = explode('{paging}', $this->l('list_show_entries')); ?>
                            <?php echo $show_lang_string; ?>
                        </div>
                        <select name="per_page" class="per_page form-control input-sm">
                            <?php foreach ($paging_options as $option) { ?>
                                <option value="<?php echo $option; ?>" <?php if ($option == $default_per_page) { ?>selected="selected" <?php } ?>>
                                    <?php echo $option; ?>&nbsp;&nbsp;
                                </option>
                            <?php } ?>
                        </select>
                        <div class="input-group-addon">
                            <?php echo $entries_lang_string; ?>
                        </div>
                    </div> -->



                            <div class="floatL t10 only-desktops" style="margin-top: 6px;">
                                <?php list($show_lang_string, $entries_lang_string) = explode('{paging}', $this->l('list_show_entries')); ?>
                                <?php echo $show_lang_string; ?>
                            </div>
                            <div class="floatL r5 l5  only-desktops">
                                <select name="per_page" class="per_page form-control input-sm">
                                    <?php foreach ($paging_options as $option) { ?>
                                        <option value="<?php echo $option; ?>" <?php if ($option == $default_per_page) { ?>selected="selected" <?php } ?>>
                                            <?php echo $option; ?>&nbsp;&nbsp;
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="floatL t10  only-desktops" style="margin-top: 6px;">
                                <?php echo $entries_lang_string; ?>
                            </div>
                            <div class="clear"></div>
                        </div>
                    <?php } ?>
                    <?php if ($total_results > 10) { ?>
                        <div class="floatR r5">
                            <ul class="pagination" style="margin: 0px 0;">
                                <li class="disabled paging-first"><a href="#"><i class="fa fa-step-backward"></i></a></li>
                                <li class="prev disabled paging-previous"><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                <li>
                                    <span class="page-number-input-container" style="padding: 2.5px 3px !important;">
                                        <input type="number" value="1" class="form-control page-number-input" />
                                    </span>
                                </li>
                                <li class="next paging-next"><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                                <li class="paging-last"><a href="#"><i class="fa fa-step-forward"></i></a></li>
                            </ul>

                            <input type="hidden" name="page_number" class="page-number-hidden" value="1" />
                            <div class="btn-group floatR l10 settings-button-container">
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle settings-button" data-toggle="dropdown">
                                    <i class="fa fa-cog r5"></i>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="javascript:void(0)" class="clear-filtering">
                                            <i class="fa fa-eraser"></i> Clear filtering
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="floatR r10 only-desktops" style="margin-top: 0px;border: 1px dashed #dddddd;padding: 5px;border-radius: 4px;color: #82838a;">
                        <?php echo $list_displaying; ?>
                        <span class="full-total-container hidden">
                            <?php echo str_replace(
                                "{total_results}",
                                "<span class='full-total'>" . $this->get_total_results() . "</span>",
                                $this->l('list_filtered_from')
                            );
                            ?>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete confirmation dialog -->
    <div class="delete-confirmation modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><?php echo $this->l('list_delete'); ?></h4>
                </div>
                <div class="modal-body">
                    <p><?php echo $this->l('alert_delete'); ?></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->l('form_cancel'); ?></button>
                    <button type="button" class="btn btn-danger delete-confirmation-button"><?php echo $this->l('list_delete'); ?></button>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Delete confirmation dialog -->

    <!-- Delete Multiple confirmation dialog -->
    <div class="delete-multiple-confirmation modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><?php echo $this->l('list_delete'); ?></h4>
                </div>
                <div class="modal-body">
                    <p><?php echo $this->l('alert_delete'); ?></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?php echo $this->l('form_cancel'); ?>
                    </button>
                    <button type="button" class="btn btn-danger delete-multiple-confirmation-button" data-target="<?php echo $delete_multiple_url; ?>">
                        <?php echo $this->l('list_delete'); ?>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Delete Multiple confirmation dialog -->

</div>
</div>