<?php
$this->set_css($this->default_theme_path . '/bootstrap/css/common.css');
$this->set_css($this->default_theme_path . '/bootstrap/css/general.css');
$this->set_css($this->default_theme_path . '/bootstrap/css/add-edit-form.css');
$this->set_css($this->default_theme_path . '/bootstrap/css/fancybox/jquery.fancybox.css');

$this->set_js_lib($this->default_theme_path . '/bootstrap/js/jquery-plugins/jquery.form.js');
$this->set_js_lib($this->default_theme_path . '/bootstrap/js/common/cache-library.js');
$this->set_js_lib($this->default_theme_path . '/bootstrap/js/common/common.js');
$this->set_js_lib($this->default_theme_path . '/bootstrap/js/jquery-plugins/jquery.fancybox-1.3.4.js');
?>
<div class="crud-form" data-unique-hash="<?php echo $unique_hash; ?>">
    <div class="gc-container">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <?php echo $this->l('list_view'); ?> <?php echo $subject ?>
                </h3>
                <div class="box-tools pull-right only-desktops">
                    <button class="btn btn-default btn-sm minimize-maximize-container minimize-maximize">
                        <i class="fa fa-caret-up"></i>
                    </button>
                    <button class="btn btn-default btn-sm gc-full-width">
                        <i class="fa fa-expand"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <?php echo form_open($update_url, 'method="post" id="crudForm"  enctype="multipart/form-data" class="form-horizontal"'); ?>
                <table class="table table-striped table-form table-mobile">
                    <?php foreach ($fields as $field) { ?>
                        <tr class='form-field-box' id="<?php echo $field->field_name; ?>_field_box">
                            <th class='form-display-as-box' style="width:25% !important;" id="<?php echo $field->field_name; ?>_display_as_box">
                                <?php echo $input_fields[$field->field_name]->display_as .
                                    ($input_fields[$field->field_name]->required ? "*" : ""); ?>
                            </th>
                            <td class='form-input-box' id="<?php echo $field->field_name; ?>_input_box">
                                <?php echo $input_fields[$field->field_name]->input ?>
                            </td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td></td>
                        <td>
                            <?php if (!$this->unset_back_to_list) { ?>
                                <button class="btn btn-default btn-sm cancel-button" type="button" onclick="window.location = '<?php echo $list_url; ?>'">
                                    <i class="fa fa-arrow-left left"></i>
                                    <?php echo $this->l('form_back_to_list'); ?>
                                </button>
                            <?php } ?>
                        </td>
                    </tr>
                </table>
                <?php if (!empty($hidden_fields)) { ?>
                    <?php
                    foreach ($hidden_fields as $hidden_field) {
                        echo $hidden_field->input;
                    }
                    ?>
                    <!-- End of hidden inputs -->
                <?php } ?>
                <?php if ($is_ajax) { ?><input type="hidden" name="is_ajax" value="true" /><?php } ?>
                <div id='report-error' class='report-div error'></div>
                <div id='report-success' class='report-div success'></div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    var validation_url = '<?php echo $validation_url ?>';
    var list_url = '<?php echo $list_url ?>';

    var message_alert_edit_form = "<?php echo $this->l('alert_edit_form') ?>";
    var message_update_error = "<?php echo $this->l('update_error') ?>";
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".fancybox").fancybox();
    });
</script>