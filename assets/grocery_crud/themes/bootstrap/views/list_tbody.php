<?php
//Start counting the buttons that we have:
$buttons_counter = 0;

if (!$unset_edit) {
    $buttons_counter++;
}

if (!$unset_read) {
    $buttons_counter++;
}

if (!$unset_delete) {
    $buttons_counter++;
}

if (!empty($list[0]) && !empty($list[0]->action_urls)) {
    $buttons_counter = $buttons_counter +  count($list[0]->action_urls);
}

$show_more_button  = $buttons_counter > 2 ? true : false;

//The more lang string exists only in version 1.5.2 or higher
$more_string =
    preg_match('/1\.(5\.[2-9]|[6-9]\.[0-9])/', Grocery_CRUD::VERSION)
    ? $this->l('list_more') : "More";

?>

<?php foreach ($list as $num_row => $row) { ?>
    <tr>


        <?php foreach ($columns as $column) { ?>
            <td>
                <?php echo $row->{$column->field_name} != '' ? $row->{$column->field_name} : '&nbsp;'; ?>
            </td>
        <?php } ?>
        <td <?php if ($unset_delete) { ?> style="border-left: none;" <?php } ?> <?php if ($buttons_counter === 0) { ?>class="hidden" <?php } ?>>
            <div class="only-desktops" style="white-space: nowrap">
                <div class="btn-group" style="display: inline-flex;">
                    <?php if (!empty($row->action_urls)) {
                        foreach ($row->action_urls as $action_unique_id => $action_url) {
                            $action = $actions[$action_unique_id]; ?>
                            <a href="<?php echo $action_url; ?>" class="btn btn-link btn-xs" <?= $action->_blank == true ? 'target="_blank"' : '' ?>>
                                <i class="<?php echo $action->css_class; ?>"></i> <?php echo $action->label ?>
                            </a>
                        <?php } ?>
                    <?php } ?>
                    <?php if (!$unset_read) { ?>
                        <a class="btn btn-link btn-xs" href="<?php echo $row->read_url ?>">
                            <i class="icon-eye8"></i>
                        </a>
                    <?php } ?>
                    <?php if (!$unset_edit) { ?>
                        <a class="btn btn-link btn-xs" href="<?php echo $row->edit_url ?>">
                            <i class="icon-pencil7 left"></i> <?php echo $this->l('list_edit'); ?>
                        </a>
                    <?php } ?>

                    <?php if (!$unset_delete) { ?>
                        <a data-target="<?php echo $row->delete_url ?>" href="javascript:void(0)" title="<?php echo $this->l('list_delete') ?>" class="btn btn-link btn-xs delete-row">
                            <i class="icon-trash"></i>
                        </a>
                    <?php } ?>
                </div>
            </div>
            <div class="only-mobiles">
                <?php if ($buttons_counter > 0) { ?>
                    <?php if (!$unset_edit) { ?>
                        <a href="<?php echo $row->edit_url ?>">
                            <i class="fa fa-pencil"></i> <?php echo $this->l('list_edit'); ?>
                        </a>
                    <?php } ?>
                    <?php
                    if (!empty($row->action_urls)) {
                        foreach ($row->action_urls as $action_unique_id => $action_url) {
                            $action = $actions[$action_unique_id];
                    ?>
                            <a href="<?php echo $action_url; ?>" <?= $action->_blank == true ? 'target="_blank"' : '' ?>>
                                <i class="fa <?php echo $action->css_class; ?>"></i> <?php echo $action->label ?>
                            </a>
                    <?php }
                    }
                    ?>
                    <?php if (!$unset_read) { ?>
                        <a href="<?php echo $row->read_url ?>">
                            <i class="icon-eye left"></i>
                        </a>
                    <?php } ?>
                    <?php if (!$unset_edit) { ?>
                        <a href="<?php echo $row->edit_url ?>">
                            <i class="icon-pencil7 left"></i>
                        </a>
                    <?php } ?>
                    <?php if (!$unset_delete) { ?>
                        <a data-target="<?php echo $row->delete_url ?>" href="javascript:void(0)" title="<?php echo $this->l('list_delete') ?>" class="delete-row">
                            <i class="icon-trash left"></i>
                        </a>
                    <?php } ?>
                <?php } ?>
            </div>
        </td>
    </tr>
<?php } ?>