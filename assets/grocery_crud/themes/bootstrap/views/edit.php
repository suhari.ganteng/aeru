<?php
$this->set_css($this->default_theme_path . '/bootstrap/css/common.css');
$this->set_css($this->default_theme_path . '/bootstrap/css/general.css');
$this->set_css($this->default_theme_path . '/bootstrap/css/add-edit-form.css');

$this->set_js_lib($this->default_theme_path . '/bootstrap/js/jquery-plugins/jquery.form.min.js');
$this->set_js_lib($this->default_theme_path . '/bootstrap/js/common/common.min.js');
$this->set_js_config($this->default_theme_path . '/bootstrap/js/form/edit.js');
$this->set_js_lib($this->default_theme_path . '/bootstrap/js/common/cache-library.js');
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/jquery.noty.js');
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/config/jquery.noty.config.js');

?>
<div class="crud-form" data-unique-hash="<?php echo $unique_hash; ?>">
    <div class=" gc-container">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="icon-database-edit2" style="font-size: 18px;margin-right: 5px;margin-top: -2px;"></i> <?php echo $this->l('form_edit'); ?> <?php echo $subject ?>
                </h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-default btn-sm minimize-maximize-container minimize-maximize">
                        <i class="fa fa-caret-up"></i>
                    </button>
                    <button class="btn btn-default btn-sm gc-full-width">
                        <i class="fa fa-expand"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <?php echo form_open($update_url, 'method="post" id="crudForm"  enctype="multipart/form-data" class="form-horizontal"'); ?>

                <table class="table table-form table-mobile">
                    <?php foreach ($fields as $field) { ?>
                        <tr id="<?php echo $field->field_name; ?>_field_box">
                            <th id="<?php echo $field->field_name; ?>_display_as_box">
                                <?php echo $input_fields[$field->field_name]->display_as .
                                    ($input_fields[$field->field_name]->required ? "*" : ""); ?>
                            </th>
                            <td id="<?php echo $field->field_name; ?>_input_box">
                                <?php echo $input_fields[$field->field_name]->input ?>
                            </td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td></td>
                        <td>
                            <?php if (isset($_GET['redirect'])) { ?>
                                <button class="btn bg-blue btn-sm " type="submit" id="form-button-save">
                                    <i class="fa fa-check left"></i>
                                    <?php echo $this->l('form_update_changes'); ?>
                                </button>
                                <a href="<?= base_url($_GET['redirect']) ?>" class="btn btn-default btn-sm">
                                    <i class="fa fa-arrow-left left"></i>
                                    <?php echo $this->l('form_cancel'); ?>
                                </a>
                            <?php } else { ?>
                                <?php if (!$this->unset_back_to_list) { ?>
                                    <button class="btn btn-primary btn-sm " type="submit" id="form-button-save">
                                        <i class="fa fa-check left"></i>
                                        <?php echo $this->l('form_save'); ?>
                                    </button>
                                <?php } ?>

                                <button class="btn btn-primary  btn-sm " type="button" id="save-and-go-back-button">
                                    <i class="fa fa-history left"></i>
                                    <?php echo $this->l('form_save_and_go_back'); ?>
                                </button>
                                <button class="btn btn-default  btn-sm cancel-button " type="button" id="cancel-button">
                                    <i class="fa fa-arrow-left left"></i>
                                    <?php echo $this->l('form_cancel'); ?>
                                </button>
                            <?php } ?>
                        </td>
                    </tr>
                </table>

                <?php if (!empty($hidden_fields)) { ?>
                    <!-- Start of hidden inputs -->
                    <?php
                    foreach ($hidden_fields as $hidden_field) {
                        echo $hidden_field->input;
                    }
                    ?>
                    <!-- End of hidden inputs -->
                <?php } ?>
                <?php if ($is_ajax) { ?><input type="hidden" name="is_ajax" value="true" /><?php } ?>
                <div id='report-error' class='alert' style="display:none;border: 1px dashed red;"></div>
                <div id='report-success' class='alert alert-success' style="display:none"></div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    var validation_url = '<?php echo $validation_url ?>';
    var list_url = '<?php echo $list_url ?>';

    var message_alert_edit_form = "<?php echo $this->l('alert_edit_form') ?>";
    var message_update_error = "<?php echo $this->l('update_error') ?>";
</script>