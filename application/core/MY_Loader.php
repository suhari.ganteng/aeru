<?php
class MY_Loader extends CI_Loader
{

	public function yii_view($view, $vars = array(), $return = FALSE)
	{
		$output = $this->_ci_load(array(
			'_ci_view' => $view,
			'_ci_vars' => ($vars),
			'_ci_return' => true
		));

		Yii::app()->getClientScript()->render($output);

		if ($return)
			return $output;

		echo $output;
	}

	public function yii_view_partial($view, $vars = array())
	{
		$output = $this->_ci_load(array(
			'_ci_view' => $view,
			'_ci_vars' => ($vars),
			'_ci_return' => true
		));
		Yii::app()->getClientScript()->render($output);
		echo $output;
	}

	function ext_view($folder, $view, $vars = array(), $return = FALSE)
	{

		$this->_ci_view_paths = array_merge($this->_ci_view_paths, array(
			APPPATH . $folder . '/' => TRUE
		));

		return $this->_ci_load(array(
			'_ci_view' => $view,
			'_ci_vars' => $this->_ci_object_to_array($vars),
			'_ci_return' => $return
		));
	}

	public function widget($className, $options = array())
	{
		$widget = new $className();
		foreach ($options as $key => $value)
			$widget->$key = $value;

		$widget->init();

		return $widget->run();
	}

	public function createWidget($className, $properties = array())
	{
		$widget = Yii::app()->getWidgetFactory()->createWidget($this, $className, $properties);
		$widget->init();
		return $widget;
	}

	public function beginWidget($className, $properties = array())
	{
		$widget = $this->createWidget($className, $properties);
		$this->_widgetStack[] = $widget;
		return $widget;
	}


	public function endWidget($id = '')
	{
		if (($widget = array_pop($this->_widgetStack)) !== null) {
			$widget->run();
			return $widget;
		} else
			throw new CException(Yii::t(
				'yii',
				'{controller} has an extra endWidget({id}) call in its view.',
				array('{controller}' => get_class($this), '{id}' => $id)
			));
	}
}
