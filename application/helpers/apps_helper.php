<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function set_css($a)
{
    return '<link rel="stylesheet" href="' . base_url() . 'assets/' . $a . '">';
}

function set_js($a)
{
    return '<script src="' . base_url() . 'assets/' . $a . '"></script>';
}

function asset_url($file)
{
    if (strpos($file, 'file/') !== false) {
        return base_url('downloader?path=' . ASSETPATH . '/' . $file);
    }

    return base_url('assets/' . $file);
}

function pre($data)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

if (!function_exists('get_instance')) {
    function get_instance()
    {
        $CI = &get_instance();
    }
}

if (!function_exists('getHashedPassword')) {
    function getHashedPassword($plainPassword)
    {
        return password_hash($plainPassword, PASSWORD_DEFAULT);
    }
}

if (!function_exists('verifyHashedPassword')) {
    function verifyHashedPassword($plainPassword, $hashedPassword)
    {
        return password_verify($plainPassword, $hashedPassword) ? true : false;
    }
}

function iconMoon($icon, $class)
{
    return "<i class='icon-" . $icon . " " . $class . "'></i>";
}

function iconFA($icon, $class)
{
    return "<i class='icon-" . $icon . " " . $class . "'></i>";
}

function getTahunAkademik()
{
    $bulan = date('m');
    if ($bulan > 6) {
        return  date('Y') . '/' . (int) date('Y');
    } else {
        return  (int) date('Y') . '/' . date('Y');
    }
}

function  summernoteOptions()
{
    return
        [
            'toolbar' => [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['para', ['ul', 'ol']],
                'help'
            ],
        ];
}

function dropdownKodeCPMK()
{
    $return = [
        'CPMK-1' => 'CPMK-1',
        'CPMK-2' => 'CPMK-2',
        'CPMK-3' => 'CPMK-3',
        'CPMK-4' => 'CPMK-4',
        'CPMK-5' => 'CPMK-5',
        'CPMK-6' => 'CPMK-6',
        'CPMK-7' => 'CPMK-7',
        'CPMK-8' => 'CPMK-8',
        'CPMK-9' => 'CPMK-9',
        'CPMK-10' => 'CPMK-10',
        'CPMK-11' => 'CPMK-11',
        'CPMK-12' => 'CPMK-12',
        'CPMK-13' => 'CPMK-13',
        'CPMK-14' => 'CPMK-14',
    ];

    return $return;
}

function dropdownKodeSubCPMK()
{
    $return = [
        'Sub-CPMK-1' => 'Sub-CPMK-1',
        'Sub-CPMK-2' => 'Sub-CPMK-2',
        'Sub-CPMK-3' => 'Sub-CPMK-3',
        'Sub-CPMK-4' => 'Sub-CPMK-4',
        'Sub-CPMK-5' => 'Sub-CPMK-5',
        'Sub-CPMK-6' => 'Sub-CPMK-6',
        'Sub-CPMK-7' => 'Sub-CPMK-7',
        'Sub-CPMK-8' => 'Sub-CPMK-8',
        'Sub-CPMK-9' => 'Sub-CPMK-9',
        'Sub-CPMK-10' => 'Sub-CPMK-10',
        'Sub-CPMK-11' => 'Sub-CPMK-11',
        'Sub-CPMK-12' => 'Sub-CPMK-12',
        'Sub-CPMK-13' => 'Sub-CPMK-13',
        'Sub-CPMK-14' => 'Sub-CPMK-14',
    ];

    return $return;
}



if (!function_exists('bulanID($value)')) {
    function bulanID($value)
    {
        setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
        return strftime('%B', mktime(0, 0, 0, $value));
    }
}

function hariID($d, $m, $y)
{
    setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
    return strftime('%A', mktime(0, 0, 0, $m, $d, $y));
}

function hariIDFromDate($date)
{
    setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
    return  strftime("%A", strtotime($date));
}

if (!function_exists('dateId()')) {
    function dateId()
    {
        setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
        return  strftime("%d %B %Y");
    }
}

function toDateID($date)
{
    setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
    return  strftime("%d %B %Y", strtotime($date));
}

if (!function_exists('penyebut($nilai)')) {
    function penyebut($nilai)
    {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " " . $huruf[$nilai];
        } else if ($nilai < 20) {
            $temp = penyebut($nilai - 10) . " belas";
        } else if ($nilai < 100) {
            $temp = penyebut($nilai / 10) . " puluh" . penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = penyebut($nilai / 100) . " ratus" . penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = penyebut($nilai / 1000) . " ribu" . penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = penyebut($nilai / 1000000) . " juta" . penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = penyebut($nilai / 1000000000) . " milyar" . penyebut(fmod($nilai, 1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = penyebut($nilai / 1000000000000) . " trilyun" . penyebut(fmod($nilai, 1000000000000));
        }
        return $temp;
    }
}

if (!function_exists('terbilang($nilai)')) {
    function terbilang($nilai)
    {
        if ($nilai < 0) {
            $hasil = "minus " . trim(penyebut($nilai));
        } else {
            $hasil = trim(penyebut($nilai));
        }
        return $hasil;
    }
}

function dropdownHari()
{
    return [
        'Senin' => 'Senin',
        'Selasa' => 'Selasa',
        'Rabu' => 'Rabu',
        'Kamis' => 'Kamis',
        'Jumat' => 'Jumat',
        'Sabtu' => 'Sabtu',
        'Minggu' => 'Minggu'
    ];
}

function isJadwalPresensi()
{
    $idPeriodeAkademik = get_instance()->session->userdata('user_institution_periode_id');
    $event = TrefEventAkademik::model()->findByAttributes(['flag_presensi' => true]);
    if ($event) {
        $presensi = TranEventAkademik::model()->findByAttributes(['id_periode_akademik' => $idPeriodeAkademik, 'id_event_akademik' => $event->id]);
        if ($presensi) {
            if (date('Y-m-d') > $presensi->tanggal_selesai) {
                return false; // event presesnsi terlwat
            } else {
                return true; // event presesnsi didalam event
            }
        } else {
            return false; // tidak ada event
        }
    } else {
        return false; // tidak ada event
    }
}
