<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function widgetStackTable($tableClass)
{
    echo set_css('public/plugins/stacktable/stacktable.css');
    echo set_js('public/plugins/stacktable/stacktable.js');

    echo '<script>
            $("' . $tableClass . '").cardtable();
            $(document).ajaxStart(function() {
            }).ajaxStop(function() {
                $("' . $tableClass . '").removeClass("stacktable large-only");
                $("' . $tableClass . '").cardtable();
            })
        </script>';
}

function widgetConfrimAssets()
{
    echo set_css('public/plugins/jquery-confirm/jquery-confirm.min.css');
    echo set_js('public/plugins/jquery-confirm/jquery-confirm.min.js');
}

function widgetConfrimDelete($title, $content, $okConfrim)
{
    echo "
    $.confirm({
        title: '" . $title . "',
        content: '" . $content . "',
        buttons: {
            confirm: function () {
                " . $okConfrim . "
            },
            cancel: function () {},
        }
    });
    ";
}
