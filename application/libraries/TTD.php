<?php

class TTD
{

    public static function gchart($idPengguna)
    {
        $model = TmstPengguna::model()->findByPk($idPengguna);
        $contact =
            "
BEGIN:VCARD
VERSION:3.0
N: " . $model->nama . "
ORG: Universitas Buana Perjuangan
EMAIL;TYPE=INTERNET:" . $model->email . "
END:VCARD
";

        $ttd = '<img style="width:60px ;" src="https://chart.googleapis.com/chart?chs=400x400&cht=qr&choe=UTF-8&chl=' . urlencode($contact) . '">';
        $body = '<table border="1" class="deskripsi-table">
            <tr>
            <td>DIBUAT</td>
            <td>' . $ttd . '</td>
            </tr>
        </table>';
        return $ttd;
    }

    public static function gchartMHS($nama)
    {
        $contact =
            "
BEGIN:VCARD
VERSION:3.0
N: " . $nama . "
ORG: Universitas Buana Perjuangan
END:VCARD
";

        $ttd = '<img style="width:60px;" src="https://chart.googleapis.com/chart?chs=400x400&cht=qr&choe=UTF-8&chl=' . urlencode($contact) . '">';
        $body = '<table border="1" class="deskripsi-table">
            <tr>
            <td>DIBUAT</td>
            <td>' . $ttd . '</td>
            </tr>
        </table>';
        return $ttd;
    }
}
