<?php defined('BASEPATH') or exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;
use setasign\Fpdi\Tcpdf\Fpdi;

class BaseController extends CI_Controller
{
	function isLoggedIn()
	{
		$isLoggedIn = $this->session->userdata('isLoggedIn');
		if (!isset($isLoggedIn) || $isLoggedIn != TRUE) {
			redirect('auth/login');
		}
	}

	function loadViews($viewName = "", $pageInfo = "", $echo = false)
	{
		$this->load->view('layouts/header', $pageInfo);
		$this->load->view($viewName);
		$this->load->view('layouts/footer');
	}

	function generate_crud($table, $subject = '')
	{
		// create CRUD object
		$this->load->library('Grocery_CRUD');
		$this->load->library('Ajax_Grocery_CRUD');
		$crud = new Ajax_Grocery_CRUD();
		$crud->set_table($table);
		$crud->set_theme('bootstrap');
		$crud->set_language('indonesian');
		$crud->set_subject($subject);
		$crud->unset_jquery();
		return $crud;
	}

	function generate_crud_without_table($subject = '')
	{
		// create CRUD object
		$this->load->library('Grocery_CRUD');
		$this->load->library('Ajax_Grocery_CRUD');
		$crud = new Ajax_Grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_language('indonesian');
		$crud->set_subject($subject);
		$crud->unset_jquery();
		return $crud;
	}

	function loadDenied()
	{
		$this->loadViews('page/denied', NULL);
	}

	function loadNotFound()
	{
		$this->loadViews('page/404', NULL);
	}

	function loadError($message)
	{
		$this->loadViews('page/error', ['message' => $message]);
	}

	function sendEmail($data = array())
	{
		// $data['email'] = ['ucenxyz@gmail.com'];

		foreach ($data['email'] as $key => $email) {
			if (isset($data['detail'])) {
				$content =  ['message' => $data['message'], 'detail' => $data['detail']];
			} else {
				$content =  ['message' => $data['message']];
			}

			$template = $this->load->view('template/mail/layout', $content, true);

			$transport = (new Swift_SmtpTransport('smtp.gmail.com', 465, "ssl"))->setUsername('pembelajaran@ubpkarawang.ac.id')->setPassword('pembelajaran2022');

			$mailer = new Swift_Mailer($transport);
			$logger = new \Swift_Plugins_Loggers_ArrayLogger();
			$mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));

			$message = (new Swift_Message('Sistem Pembelajaran Notifikasi'))->setFrom(['pembelajaran@ubpkarawang.ac.id' => 'Sistem Pemebelajaran'])->setTo([$email => null])->setBody($template, 'text/html');
			$mailer->send($message, $errors);
		}
	}


	function renderPartial($views, $var, $return, $processOutput)
	{
		$yii = new CController(0);
		$yii->renderPartial($views, $var, $return, $processOutput);
	}

	function modelValidation($model)
	{
		if (isset($_POST['ajax'])) {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
