<?php

class UGridView extends CGridView
{
    public $pager           = array('class' => 'UPager');
    public $pagerCssClass   = 'text-center';
    public $itemsCssClass   = 'items';

    public function init()
    {
        parent::init();

        $this->htmlOptions['class'] = 'grid-view table-responsive';

        if (!isset($this->htmlOptions['class']))
            $this->htmlOptions['class'] = 'grid-view';

        if ($this->baseScriptUrl === null)
            $this->baseScriptUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview';

        if ($this->cssFile !== false) {
            if ($this->cssFile === null)
                $this->cssFile = $this->baseScriptUrl . '/styles.css';
            // Yii::app()->getClientScript()->registerCssFile($this->cssFile);
        }

        // $this->initColumns();
        $this->loadStacktableAssets();
    }

    // table responsive mobile
    public function loadStacktableAssets()
    {
        Yii::app()->getClientScript()->registerCssFile(asset_url('public/plugins/stacktable/stacktable.css'));
        Yii::app()->getClientScript()->registerScriptFile(asset_url('public/plugins/stacktable/stacktable.js'), CClientScript::POS_END);
        Yii::app()->getClientScript()->registerCoreScript('bbq', CClientScript::POS_END);
        Yii::app()->clientScript->registerScript('stacktableGridView', "
            $('.grid-view table').cardtable();
            $(document).ajaxStart(function() {
                // Pace.restart()
            }).ajaxStop(function() {
                $('.grid-view table').cardtable();
            })
        ", CClientScript::POS_END);
    }
}
