<?php
class UPager extends CLinkPager
{
	const CSS_FIRST_PAGE = 'first';
	const CSS_LAST_PAGE = 'last';
	const CSS_PREVIOUS_PAGE = 'previous';
	const CSS_NEXT_PAGE = 'next';
	const CSS_INTERNAL_PAGE = 'page';
	const CSS_HIDDEN_PAGE = 'disabled';
	const CSS_SELECTED_PAGE = 'active';


	public $maxButtonCount = 5;
	public $nextPageLabel = '<i class="fa fa-angle-right"></i>';
	public $prevPageLabel = '<i class="fa fa-angle-left"></i>';
	public $firstPageLabel = '<i class="fa fa-angle-double-left"></i>';
	public $lastPageLabel = '<i class="fa fa-angle-double-right"></i>';
	public $header = '';
	public $footer = '';
	public $cssFile;
	public $htmlOptions = array();
	public function init()
	{

		if (!isset($this->htmlOptions['id'])) $this->htmlOptions['id'] = $this->getId();
		if (!isset($this->htmlOptions['class'])) $this->htmlOptions['class'] = 'pagination pagination-sm';
	}


	public function run()
	{
		$this->registerClientScript();
		$buttons = $this->createPageButtons();
		if (empty($buttons))
			return;
		echo $this->header;
		echo CHtml::tag('ul', $this->htmlOptions, implode("\n", $buttons));
		echo $this->footer;
	}


	protected function createPageButtons()
	{
		if (($pageCount = $this->getPageCount()) <= 1)
			return array();

		list($beginPage, $endPage) = $this->getPageRange();
		$currentPage = $this->getCurrentPage(false);
		$buttons = array();


		$buttons[] = $this->createPageButton($this->firstPageLabel, 0, self::CSS_FIRST_PAGE, $currentPage <= 0, false);


		if (($page = $currentPage - 1) < 0)
			$page = 0;
		$buttons[] = $this->createPageButton($this->prevPageLabel, $page, self::CSS_PREVIOUS_PAGE, $currentPage <= 0, false);


		for ($i = $beginPage; $i <= $endPage; ++$i)
			$buttons[] = $this->createPageButton($i + 1, $i, self::CSS_INTERNAL_PAGE, false, $i == $currentPage);


		if (($page = $currentPage + 1) >= $pageCount - 1)
			$page = $pageCount - 1;
		$buttons[] = $this->createPageButton($this->nextPageLabel, $page, self::CSS_NEXT_PAGE, $currentPage >= $pageCount - 1, false);


		$buttons[] = $this->createPageButton($this->lastPageLabel, $pageCount - 1, self::CSS_LAST_PAGE, $currentPage >= $pageCount - 1, false);

		return $buttons;
	}


	protected function createPageButton($label, $page, $class, $hidden, $selected)
	{
		if ($hidden || $selected)
			$class .= ' ' . ($hidden ? self::CSS_HIDDEN_PAGE : self::CSS_SELECTED_PAGE);
		return '<li class="' . $class . '">' . CHtml::link($label, $this->createPageUrl($page)) . '</li>';
	}


	protected function getPageRange()
	{
		$currentPage = $this->getCurrentPage();
		$pageCount = $this->getPageCount();

		$beginPage = max(0, $currentPage - (int) ($this->maxButtonCount / 2));
		if (($endPage = $beginPage + $this->maxButtonCount - 1) >= $pageCount) {
			$endPage = $pageCount - 1;
			$beginPage = max(0, $endPage - $this->maxButtonCount + 1);
		}
		return array($beginPage, $endPage);
	}


	public function registerClientScript()
	{
		if ($this->cssFile !== false)
			self::registerCssFile($this->cssFile);
	}


	public static function registerCssFile($url = null)
	{
		if ($url === null)
			$url = CHtml::asset(Yii::getPathOfAlias('system.web.widgets.pagers.pager') . '.css');
		Yii::app()->getClientScript()->registerCssFile($url);
	}
}
