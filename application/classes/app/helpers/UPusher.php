<?php

class UPusher
{
    public static function send($message)
    {
        $options = array(
            'cluster' => 'ap1',
            'useTLS' => true
        );
        $pusher = new Pusher\Pusher(
            'd258ba3e99901ccbf855',
            '3bc9753012cfe109b05e',
            '1473024',
            $options
        );

        $pusher->trigger(get_instance()->session->userdata('user_email'), 'my-event', $message);
    }

    public static function log($m)
    {
        $message = '<pre>' . $m . '</pre>';
        UPusher::send($message);
    }
}
