<?php

/**
 * This is the model class for table "tmst_risiko".
 *
 * The followings are the available columns in table 'tmst_risiko':
 * @property integer $id
 * @property string $nomor_njk
 * @property string $judul_njk
 * @property string $profile
 * @property string $mitigasi
 * @property string $monitoring
 * @property string $updress_proyek
 */
class TrefRisiko extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tref_risiko';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pro_risk, mitigasi', 'required'),
			array('mitigasi, monitoring, proyek', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('pro_risk, mitigasi, monitoring, proyek', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		
		return array(
			'idRisk' => array(self::BELONGS_TO, 'TmstNota', 'id_nota')
			//'tmstNotas' => array(self::HAS_MANY, 'TmstNota', 'id_nota'),
			//'idNota' => array(self::BELONGS_TO, 'TmstNota', 'id_nota'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_nota' => 'ID',
			'pro_risk' => 'Profile Risiko',
			'mitigasi' => 'Mitigasi',
			'monitoring' => 'Monitoring',
			'proyek' => 'Update Progres Proyek',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('pro_risk',$this->pro_risk,true);
		$criteria->compare('mitigasi',$this->mitigasi,true);
		$criteria->compare('monitoring',$this->monitoring,true);
		$criteria->compare('proyek',$this->proyek,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TmstRisiko the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
