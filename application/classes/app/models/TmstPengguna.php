<?php

/**
 * This is the model class for table "tmst_pengguna".
 *
 * The followings are the available columns in table 'tmst_pengguna':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $nama
 * @property string $np
 * @property integer $flag_aktif
 * @property string $last_login
 *
 * The followings are the available model relations:
 * @property TranPenggunaSistem[] $tranPenggunaSistems
 */
class TmstPengguna extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tmst_pengguna';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username', 'required'),
			array('flag_aktif', 'numerical', 'integerOnly'=>true),
			array('username, nama', 'length', 'max'=>50),
			array('password', 'length', 'max'=>80),
			array('np', 'length', 'max'=>20),
			array('last_login', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, password, nama, np, flag_aktif, last_login', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tranPenggunaSistems' => array(self::HAS_MANY, 'TranPenggunaSistem', 'id_pengguna'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' 			=> 'ID',
			'username' 		=> 'Username',
			'password' 		=> 'Password',
			'nama' 			=> 'Nama',
			'np' 			=> 'np',
			'flag_aktif' 	=> 'Flag Aktif',
			'last_login' 	=> 'Last Login',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('np',$this->np,true);
		$criteria->compare('flag_aktif',$this->flag_aktif);
		$criteria->compare('last_login',$this->last_login,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TmstPengguna the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
