<?php

/**
 * This is the model class for table "tmst_nota".
 *
 * The followings are the available columns in table 'tmst_nota':
 * @property integer $id
 * @property string $tanggal
 * @property string $judul
 * @property integer $id_divisi
 * @property integer $id_risiko
 * @property string $tgl_nota
 * @property string $file_nota
 * @property string $tindak_lanjut
 * @property string $nilai_proyek
 * @property string $pro_risk
 * @property string $keterangan
 * @property string $concern
 * @property string $tanggal_diperiksa
 * @property string $create_at
 *
 * The followings are the available model relations:
 * @property TranDataPendukung[] $tranDataPendukungs
 */
class TmstNota extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tmst_nota';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tanggal, judul, id_divisi, tgl_nota', 'required'),
			array('id_divisi','numerical', 'integerOnly' => true),
			array('file_nota', 'length', 'max' => 100),
			array('judul', 'length', 'max' => 100),
			array('tgl_nota', 'length', 'max' => 30),
			array('id_divisi,tanggal,judul,tgl_nota,nilai_proyek', 'safe'),
			array('no_review, keterangan, concern, target_waktu, pro_risk, mitigasi, monitoring, proyek, tindak_lanjut, tanggal_diperiksa, nilai_proyek, create_at', 'safe'),
			array('pro_risk, mitigasi, monitoring, proyek', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('tanggal, judul, id_divisi, file_nota, tgl_nota, tindak_lanjut, keterangan, concern, tanggal_diperiksa, nilai_proyek, create_at', 'safe', 'on' => 'search'),
			array('tanggal, judul, id_divisi, tgl_nota,', 'safe', 'on' => 'search'),
			array('file_nota', 'file', 'allowEmpty' => true, 'types' => 'pdf'),
			array('file_nota', 'file', 'allowEmpty' => false, 'types' => 'pdf', 'maxSize' => 10 * 1024 * 1024, 'on' => 'insertU'),
			array('file_nota', 'unsafe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idDivisi' => array(self::BELONGS_TO, 'TrefDivisi', 'id_divisi'),
			//'idRisiko' => array(self::BELONGS_TO, 'TrefRisiko', 'id'),
			'tranDataPendukungs' => array(self::HAS_MANY, 'TranDataPendukung', 'id_nota'),
			//'trefRisikos' => array(self::HAS_MANY, 'TrefRisiko', 'id_nota'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' 				=> 'No',
			'id_divisi'			=> 'Pengusul',
			'id_risk'			=> 'Risiko',
			'tanggal' 			=> 'Tanggal',
			'judul' 			=> 'Judul',
			'tgl_nota'			=> 'Nomor Nota',
			'file_nota'			=> 'File Nota',
			'file_pendukung'	=> 'Data Pendukung',
			'tindak_lanjut'		=> 'Tindak Lanjut',
			'keterangan'		=> 'Keterangan',
			'concern'			=> 'Concern',
			'no_review'			=> 'Nomor Review',
			'target_waktu'		=> 'Target Waktu',
			'pro_risk'			=> 'Profile Risiko',
			'mitigasi'			=> 'Mitigasi',
			'monitoring'		=> 'Monitoring',
			'proyek'			=> 'Progres Proyek',
			'nilai_proyek'		=> 'Nilai Proyek',
			'tanggal_diperiksa' => 'Tanggal Diperiksa',
			'target_waktu'		=> 'Target Waktu',
			'create_at'			=> 'Create At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('tanggal', $this->tanggal, true);
		$criteria->compare('judul', $this->judul, true);
		$criteria->compare('id_divisi', $this->id_divisi);
		$criteria->compare('tgl_nota', $this->tgl_nota, true);
		$criteria->compare('file_nota', $this->file_nota, true);
		//$criteria->compare('tindak_lanjut', $this->tindak_lanjut, true);
		//$criteria->compare('keterangan', $this->keterangan, true);
		//$criteria->compare('concern', $this->concern, true);
		$criteria->compare('target_waktu', $this->target_waktu, true);
		//$criteria->compare('nilai_proyek', $this->nilai_proyek, true);
		//$criteria->compare('tanggal_diperiksa', $this->tanggal_diperiksa, true);
		//$criteria->compare('create_at', $this->create_at, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TmstNota the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getLinkFileNota()
	{
		return '<a href="' . asset_url('files/nota/' . $this->file_nota) . '" data-fancybox="iframe">Lihat</a>';
	}

	public function getLinkFilePendukung()
	{
		$model = TranDataPendukung::model()->findAllByAttributes(['id_nota' => $this->id]);

		if ($model) {
			$data = array();
			foreach ($model as $key => $value) {
				$data[] = '<a href="' . asset_url('files/pendukung/' . $value->file_pendukung) . '" data-fancybox="iframe">' . $value->file_pendukung . '</a>';
			}
			return implode('<br>', $data);
		} else {
			return '-';
		}
	}
}
