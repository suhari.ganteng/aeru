<?php

/**
 * This is the model class for table "tran_data_pendukung".
 *
 * The followings are the available columns in table 'tran_data_pendukung':
 * @property integer $id
 * @property integer $id_nota
 * @property string $file
 *
 * The followings are the available model relations:
 * @property TmstNota $idNota
 */
class TranDataPendukung extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tran_data_pendukung';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_nota', 'required'),
            array('id_nota', 'numerical', 'integerOnly' => true),
            array('file_pendukung', 'length', 'max' => 100),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, id_nota', 'safe', 'on' => 'search'),

            array('file_pendukung', 'file', 'allowEmpty' => true, 'types' => 'pdf,docx,jpeg,jpg,xls'),
            array('file_pendukung', 'unsafe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'idNota' => array(self::BELONGS_TO, 'TmstNota', 'id_nota'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'                => 'ID',
            'id_nota'           => 'Id Nota',
            'file_pendukung'    => 'file_pendukung',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('id_nota', $this->id_nota);
        $criteria->compare('file_pendukung', $this->file, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TranDataPendukung the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
