<?php

/**
 * This is the model class for table "tmst_concern".
 *
 * The followings are the available columns in table 'tmst_concern':
 * @property integer $id
 * @property string $no_njk
 * @property string $nomor_review
 * @property string $tanggal_review
 * @property string $judul_nota
 * @property integer $id_divisi
 * @property string $concern
 * @property string $tindak_lanjut
 * @property string $target_waktu
 * @property string $keterangan
 *
 * The followings are the available model relations:
 * @property TrefDivisi $idDivisi
 */
class TmstConcern extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tmst_concern';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('no_njk, nomor_review, tanggal_review, judul_nota, id_divisi, concern, tindak_lanjut, target_waktu, keterangan', 'required'),
			array('id_divisi', 'numerical', 'integerOnly'=>true),
			array('no_njk, nomor_review', 'length', 'max'=>25),
			array('judul_nota, target_waktu, keterangan', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, no_njk, nomor_review, tanggal_review, judul_nota, id_divisi, concern, tindak_lanjut, target_waktu, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idDivisi' => array(self::BELONGS_TO, 'TrefDivisi', 'id_divisi'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'no_njk' => 'No Njk',
			'nomor_review' => 'Nomor Review',
			'tanggal_review' => 'Tanggal Review',
			'judul_nota' => 'Judul Nota',
			'id_divisi' => 'Id Divisi',
			'concern' => 'Concern',
			'tindak_lanjut' => 'Tindak Lanjut',
			'target_waktu' => 'Target Waktu',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('no_njk',$this->no_njk,true);
		$criteria->compare('nomor_review',$this->nomor_review,true);
		$criteria->compare('tanggal_review',$this->tanggal_review,true);
		$criteria->compare('judul_nota',$this->judul_nota,true);
		$criteria->compare('id_divisi',$this->id_divisi);
		$criteria->compare('concern',$this->concern,true);
		$criteria->compare('tindak_lanjut',$this->tindak_lanjut,true);
		$criteria->compare('target_waktu',$this->target_waktu,true);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TmstConcern the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
