<?php

/**
 * This is the model class for table "tran_pengguna_sistem".
 *
 * The followings are the available columns in table 'tran_pengguna_sistem':
 * @property integer $id
 * @property integer $id_pengguna
 * @property integer $id_role
 * @property integer $id_divisi
 *
 * The followings are the available model relations:
 * @property TmstPengguna $idPengguna
 * @property TrefRole $idRole
 * @property TrefDivisi $idDivisi
 */
class TranPenggunaSistem extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tran_pengguna_sistem';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pengguna, id_role', 'required'),
			array('id_pengguna, id_role, id_divisi', 'numerical', 'integerOnly' => true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_pengguna, id_role, id_divisi', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPengguna' => array(self::BELONGS_TO, 'TmstPengguna', 'id_pengguna'),
			'idRole' => array(self::BELONGS_TO, 'TrefRole', 'id_role'),
			'idDivisi' => array(self::BELONGS_TO, 'TrefDivisi', 'id_divisi'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' 			=> 'ID',
			'id_pengguna' 	=> 'Id Pengguna',
			'id_role' 		=> 'Id Role',
			'id_divisi' 	=> 'Id Divisi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('id_pengguna', $this->id_pengguna);
		$criteria->compare('id_role', $this->id_role);
		$criteria->compare('id_divisi', $this->id_divisi);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TranPenggunaSistem the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
