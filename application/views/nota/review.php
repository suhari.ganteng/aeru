<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Review Risiko</h3>
    </div>
    <div class="box-body">
        <?php
        $this->load->widget('UGridView', [
            'columns' => [
                [
                    'header' => 'No',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                ],
                [
                    'header' => 'Divisi',
                    // 'name'  => 'idPeriodeAkademik.tahun_akademik',
                    'value' => '$data->idDivisi->nama',
                    // 'filter' => TrefPeriodeAkademik::dropdown($model, 'periodeOfClass'),
                ],
                [
                    'header' => 'Tanggal',
                    'value' => '$data->tanggal',
                    //'name'=>'tanggal',
                    // 'filter' => '<input type="text" placeholder="adssf" name="TmstNota[tanggal]">',
                ],
                [
                    'header' => 'Judul',
                    'value' => '$data->judul',
                    //'name' => 'judul',
                    // 'type' => 'raw'
                ],
                [
                    'header' => 'File Nota',
                    'value' => '$data->linkFileNota',
                    // 'name' => 'mkOfClass',
                    'type' => 'raw'
                ],
                [
                    'header' => 'Tindak Lanjut',
                    'value' => '$data->tindak_lanjut',
                    //'name' => 'tindak_lanjut',
                    // 'type' => 'raw'
                ],
                [
                    'header' => 'Keterangan',
                    'value' => '$data->keterangan',
                    // 'type' => 'raw'
                ],
                [
                    'header' => 'Concern',
                    'value' => '$data->concern',
                    //'name' => 'tindak_lanjut',
                    // 'type' => 'raw'
                ],
                [
                    'class' => 'CButtonColumn',
                    'buttons' => array(
                        'tran-kelas_update' => array(
                            'label' => '<button class="btn btn-xs"><i class="icon-pencil"></i> Review </button>',
                            'url' => '$data->id',
                            'options' => array("class" => "fan_update", 'title' => Yii::t('admin_tran-kelas', 'Review')),
                            'visible'=>'empty($data->tindak_lanjut)  ? true : false'
                        ), 
                    ),
                    'template' => '{tran-kelas_update}',
                ]
            ],
            'dataProvider'      => $model->search(),
            //'filter'            => $model,
            'itemsCssClass'     => 'table table-bordered table-striped',
            'afterAjaxUpdate'   => 'js:function(id,data){$.bind_crud()}',
            'id'                => 'gridKelas',
            'selectableRows'    => 2,
        ]);
        ?>
    </div>
</div>

<?= widgetConfrimAssets() ?>
<div id="modalForm" class="white-popup mfp-with-anim mfp-hide"></div>
<script>
    $(function() {
        $.bind_crud = function() {
            $('.fan_update').each(function(index) {
                var id = $(this).attr('href');
                $(this).bind('click', function() {
                    $.ajax({
                        type: "POST",
                        url: "<?= Yii::app()->request->baseUrl ?>/nota/form_review/" + id,
                        success: function(data) {
                            $('#modalForm').html(data);
                            $.magnificPopup.open(optionsMagnific('#modalForm', 'gridKelas'));
                        }
                    });
                    return false;
                });
            });
           
        }
        $.bind_crud();

    })
</script>