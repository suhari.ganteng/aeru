<h3>Form Tambah Nota Justifikasi</h3>

<div class="box box-body">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'fForm',
        'action' => base_url('nota/addNota'),
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
    ));
    ?>
    <table class="table table-form table-mobile">
        <tr>
            <th><?= $form->label($model, 'Unit Kerja Pengusul'); ?></th>
                <td>
                    <?= Chosen::activeDropDownList($model, 'id_divisi', CHtml::listData(TrefDivisi::model(), 'id', 'nama'), ['prompt' => 'Pilih Unit Pengusul']); ?>
                    <?= $form->error($model, 'id_divisi'); ?>
                </td>
        </tr>

        <tr>
            <th><?= $form->label($model, 'Tanggal Nota'); ?></th>
            <td>
                <?= $form->dateField($model, 'tgl_nota', ['class' => 'form-control']); ?>
                <?= $form->error($model, 'tgl_nota'); ?>
            </td>
        </tr>

        <tr>
            <th><?= $form->label($model, 'Tanggal'); ?></th>
            <td>
                <?= $form->dateField($model, 'tanggal', ['class' => 'form-control']); ?>
                <?= $form->error($model, 'tanggal'); ?>
            </td>
        </tr>

        <tr>
            <th><?= $form->label($model, 'Judul'); ?></th>
            <td>
                <?= $form->textField($model, 'judul', ['class' => 'form-control form-control-long']) ?>
                <?= $form->error($model, 'judul'); ?>
            </td>
        </tr>

        <tr>
            <th><?= $form->label($model, 'Nota Justifikasi'); ?></th>
            <td>
                <?= $form->fileField($model, 'file_nota', ['class' => 'form-control']) ?>
                <?= $form->error($model, 'file_nota'); ?>
            </td>
        </tr>

        <tr>
            <th><?= $form->label($model, 'Data Pendukung'); ?></th>
            <td>
                <?= $form->fileField($model, 'id_data_pendukung', ['class' => 'form-control']) ?>
                <?= $form->error($model, 'id_data_pendukung'); ?>

            </td>
        </tr>

        <tr>
            <th></th>
            <td>
                <?= CHtml::submitButton('Simpan', ['class' => 'btn btn-sm btn-primary']); ?>
            </td>
        </tr>
    </table>
    <?php $this->endWidget(); ?>
</div>

<script type="text/javascript">
    $('#fForm').ajaxForm(optionsAjaxForm)
</script>