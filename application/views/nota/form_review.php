<h3>Form Review Risiko</h3>

<div class="box box-body">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'fForm',
        'action' => base_url('nota/update/'.$model->id),
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
    ));
    ?>
    <table class="table table-form table-mobile">
        <tr>
            <th>
                <?php echo $form->labelEx($model, 'Tindak Lanjut'); ?>
            </th>
            <td>
                <?php echo $form->radioButtonList($model, 'tindak_lanjut', ['Ya'=>'Ya','Ditolak'=>'Ditolak']); ?>
                <?php echo $form->error($model, 'tindak_lanjut'); ?>
            </td>
        </tr>

        <tr>
            <th>
                <?php echo $form->labelEx($model, 'Keterangan'); ?>
            </th>
            <td>
                <?php echo $form->textArea($model, 'keterangan',['class'=>'form-control form-control-long']); ?>
                <?php echo $form->error($model, 'keterangan'); ?>
            </td>
        </tr>

        <tr>
            <th>
                <?php echo $form->labelEx($model, 'Concern'); ?>
            </th>
            <td>
                <?php echo $form->textArea($model, 'concern', ['class'=>'form-control form-control-long']); ?>
                <?php echo $form->error($model, 'concern'); ?>
            </td>
        </tr>

        <tr>
            <th></th>
            <td>
                <?php echo CHtml::submitButton('Simpan', ['class' => 'btn btn-sm btn-primary']); ?>
            </td>
        </tr>
    </table>
    <?php $this->endWidget(); ?>
</div>

<script type="text/javascript">
    $('#fForm').ajaxForm(optionsAjaxForm)
</script>