<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Nota Justifikasi</h3>
        <div class="box-tools">
            <a href="<?= base_url('nota/nota_form') ?>" class="btn btn-sm btn-primary"><i class="icon-plus3 right"></i>Tambah</a>
        </div>
    </div>
    <div class="box-body">
        <?php
        $this->load->widget('UGridView', [
            'columns' => [
                [
                    'header' => 'No',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                ],
                [
                    'header' => 'Divisi',
                    'value' => '$data->idDivisi->nama',
                    'name' => 'id_divisi',
                ],
                [
                    'header' => 'Tanggal',
                    'value' => '$data->tanggal',
                    'name' => 'tanggal',
                ],
                [
                    'header' => 'Judul Nota',
                    'value' => '$data->judul',
                    'name' => 'judul',
                    'type' => 'raw',
                ],
                [
                    'header' => 'Nomor Nota',
                    'value' => '$data->tgl_nota',
                    'name' => 'tgl_nota',
                    'type' => 'raw',
                ],
                [
                    'header' => 'File Nota',
                    'value' => '$data->linkFileNota',
                    'type' => 'raw',
                ],
                [
                    'header' => 'File Pendukung',
                    'value' => '$data->linkFilePendukung',
                    'type' => 'raw',
                ],
                [
                    'header' => 'Nilai Proyek',
                    'value' => '$data->nilai_proyek',
                    'name' => 'nilai_proyek',
                    'type' => 'raw',
                ],
                [
                    'class' => 'CButtonColumn',
                    'buttons' => array(
                        'tran-update' => array(
                            'label' => '<button class="btn btn-xs"><i class="icon-pencil"></i>  </button>',
                            'url' => '$data->id',
                            'options' => array("class" => "fan_update", 'title' => Yii::t('admin_tran-kelas', 'Edit')),  
                        ),
                        'tran-delete' => array(
                            'label' => '<button class="btn btn-xs"><i class="icon-trash"></i>  </button>',
                            'url' => '$data->id',
                            'options' => array("class" => "fan_delete", 'title' => Yii::t('admin_tran-kelas', 'Delete')),
                        ),
                    ),
                    'template' => '{tran-update} {tran-delete}',
                ]
            ],
            'dataProvider'      => $model->search(),
            'filter'            => $model,
            'itemsCssClass'     => 'table table-bordered table-striped',
            'afterAjaxUpdate'   => 'js:function(id,data){$.bind_crud()}',
            'id'                => 'gridKelas',
            'selectableRows'    => 2,
        ]);
        ?>
    </div>
</div>

<?= widgetConfrimAssets() ?>
<div id="modalForm" class="white-popup mfp-with-anim mfp-hide"></div>
<script>
    $(function() {
        $.bind_crud = function() {
            $('.fan_update').each(function(index) {
                var id = $(this).attr('href');
                $(this).bind('click', function() {
                    $.ajax({
                        type: "POST",
                        url: "<?= Yii::app()->request->baseUrl ?>/nota/nota_edit/" + id,
                        success: function(data) {
                            $('#modalForm').html(data);
                            $.magnificPopup.open(optionsMagnific('#modalForm', 'gridKelas'));
                        }
                    });
                    return false;
                });
            });

            $('.fan_delete').each(function(index) {
                var id = $(this).attr('href');
                $(this).bind('click', function() {
                    $.confirm({
                        title: 'Konfirmasi !',
                        content: 'Hapus data ?',
                        buttons: {
                            confirm: function() {
                                $.get("<?= Yii::app()->request->baseUrl ?>/nota/delete/" + id, function(e) {
                                    notify(e);
                                    $.magnificPopup.close();
                                    $.fn.yiiGridView.update('gridKelas');
                                });
                            },
                            cancel: function() {},
                        }
                    });
                    return false;
                });
            });
        }
         $.bind_crud();
    });

</script>
