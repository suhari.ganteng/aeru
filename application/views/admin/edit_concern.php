<div class="box-primary">
    <div class="box-header">
        <div class="box-title">
            Edit Concern Risiko
        </div>
    </div>
    <div class="box box-body">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'action' => base_url('nota/concern_update/' . $model->id),
            'id' => 'fAnggaranUse',
            'enableAjaxValidation' => true,
        ));
        ?>
        <table class="table table-form">
            <tr>
                <th>
                    <label for="no_review">Nomor Review</label>
                </th>
                <td>
                    <?= $form->textField($model, 'no_review', ['class' => 'form-control']); ?>
                </td>
            </tr>

            <tr>
                <th>Tanggal Review</th>
                <td>
                    <?= $form->dateField($model, 'tanggal_diperiksa', ['class' => 'form-control']) ?>
                </td>
            </tr>

            <tr>
                <th>Concern</th>
                <td>
                    <?php $this->widget('SummernoteWidget', ['model' => $model, 'attribute' => 'concern', 'id' => 'summernoteModal', 'clientOptions' => summernoteOptions(), 'plugins' => []]); ?>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <?= CHtml::submitButton('Simpan', ['class' => 'btn btn-sm btn-primary']); ?>
                    <a href="<?= base_url('nota/index') ?>" class="btn btn-sm btn-default"> Kembali </a>
                </td>
            </tr>
        </table>
        <?php $this->endWidget(); ?>
    </div>
</div>
<?= widgetConfrimAssets() ?>
<script>
    var urutan = 1;
    $('#btn_tambah').click(function(e) {
        e.preventDefault()
        urutan++;

        $('#keterangan').append('\
        <div class="row" id="keterangan_list_' + urutan + '" style="margin-top:5px">\
            <div class="col-xs-8">\
                <input type="file" name="file_pendukung[]" class="form-control form-control-long">\
            </div>\
            <div class="col-xs-1">\
                <button type="button" class="pull-right btn btn-sm btn-default" onclick="hapus_keterangan(this);return false;" data-id="' + urutan + '">\
                    <i class="icon-cross"></i>\
                </button>\
            </div>\
        </div>\
        ');
    });

    function hapusPendukung(id, urutan) {
        $.confirm({
            buttons: {
                confirm: function() {
                    $.post(baseURL + 'pengusul/delete_file_pendukung/' + id, function(data) {
                        notify(data)
                        $('#keterangan_hapus_' + urutan).remove();
                    })
                },
                cancel: function() {},
            }
        });

    }

    function hapus_keterangan(e) {
        var id = e.getAttribute('data-id')
        $('#keterangan_list_' + id).remove()
    }

    $('#fAnggaranUse').ajaxForm({
        success: function(responseText, statusText, xhr, $form) {
            $.notify(responseText, {
                position: 'top center',
                className: 'success',
            });
        },
        error: function(responseText, statusText, xhr, $form) {
            $.alert(xhr);
        }
    });
</script>

<script type="text/javascript">
    $('#fForm').ajaxForm(optionsAjaxForm)
</script>