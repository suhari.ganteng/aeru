<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Tindak Lanjut Concern Risiko</h3>
    </div>
    <div class="box-body">
        <?php
        $this->load->widget('UGridView', [
            'columns' => [
                [
                    'header' => 'No',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                ],
                [
                    'header' => 'Nomor Nota Justifikasi',
                    'value' => '$data->tgl_nota',
                    'name' => 'tgl_nota',
                    'filter' => CHtml::activeTextField($model, 'tgl_nota'),
                ],
                [
                    'header' => 'Nomor Review',
                    'value' => '$data->no_review',
                    'name' => 'no_review',
                    'filter' => CHtml::activeTextField($model, 'no_review'),
                ],
                [
                    'header' => 'Tanggal Review',
                    'value' => '$data->tanggal_diperiksa',
                    'name' => 'tanggal_diperiksa',
                    'filter' => CHtml::activeTextField($model, 'tanggal_diperiksa'),
                ],
                [
                    'header' => 'Judul',
                    'value' => '$data->judul',
                    'name' => 'judul',
                    'filter' => CHtml::activeTextField($model, 'judul'),
                ],
                [
                    'header' => 'Divisi',
                    'name' => 'id_divisi',
                    'value' => '$data->idDivisi->nama',
                    'filter' => CHtml::activeDropDownList($model, "id_divisi", CHtml::listData(TrefDivisi::model()->findAll(), 'id', 'nama'), ['empty' => '-ALL-']),
                ],
                [
                    'header' => 'Concern',
                    'value' => '$data->concern',
                    'name' => 'concern',
                    'type' => 'raw'
                ],
                [
                    'header' => 'Tindak Lanjut',
                    'value' => '$data->tindak_lanjut',
                    'name' => 'tindak_lanjut',
                    'type' => 'raw',
                ],
                [
                    'header' => 'Target Waktu',
                    'value' => '$data->target_waktu',
                    'name' => 'target_waktu',
                ],
                [
                    'header' => 'Keterangan',
                    'value' => '$data->keterangan',
                    'name' => 'keterangan',
                    'type' => 'raw'
                ],
                [
                    'class' => 'CButtonColumn',
                    'buttons' => array(
                        'tran-update' => array(
                            'label' => '<button class="btn btn-xs"><i class="icon-pencil"></i></button>',
                            'url' => '$data->id',
                            'options' => array("class" => "fan_update", 'title' => 'Edit'),
                        ),
                        'tran-delete' => array(
                            'label' => '<button class="btn btn-xs"><i class="icon-trash"></i>  </button>',
                            'url' => '$data->id',
                            'options' => array("class" => "fan_delete", 'title' => Yii::t('admin_tref-risiko', 'Delete')),
                        ),
                        'export' => array(
                            'label' => '<button class="btn btn-xs"><i class="fas fa-file-excel"></i></button>',
                            'url' => 'base_url("nota/export/".$data->id)',
                            'options' => array("class" => "_", 'title' => 'Export'),
                        ),
                    ),
                    'template' => '{tran-update} {tran-delete} {export}',
                ]
            ],
            'dataProvider'      => $model->search(),
            'filter'            => $model,
            'itemsCssClass'     => 'table table-bordered table-striped',
            'afterAjaxUpdate'   => 'js:function(id,data){$.bind_crud()}',
            'id'                => 'gridKelas',
            'selectableRows'    => 2,
        ]);
        ?>
    </div>
</div>

<?= widgetConfrimAssets() ?>
<div id="modalForm" class="white-popup mfp-with-anim mfp-hide"></div>
<script>
    $(function() {
        $.bind_crud = function() {
            $('.fan_update').each(function(index) {
                var id = $(this).attr('href');
                $(this).bind('click', function() {
                    $.ajax({
                        type: "POST",
                        url: "<?= Yii::app()->request->baseUrl ?>/nota/concern_edit/" + id,
                        success: function(data) {
                            $('#modalForm').html(data);
                            $.magnificPopup.open(optionsMagnific('#modalForm', 'gridKelas'));
                        }
                    });
                    return false;
                });
            });

            $('.fan_delete').each(function(index) {
                var id = $(this).attr('href');
                $(this).bind('click', function() {
                    $.confirm({
                        title: 'Konfirmasi !',
                        content: 'Hapus data ?',
                        buttons: {
                            confirm: function() {
                                $.get("<?= Yii::app()->request->baseUrl ?>/nota/delete/" + id, function(e) {
                                    notify(e);
                                    $.magnificPopup.close();
                                    $.fn.yiiGridView.update('gridKelas');
                                });
                            },
                            cancel: function() {},
                        }
                    });
                    return false;
                });
            });
        }
        $.bind_crud();
    });
</script>