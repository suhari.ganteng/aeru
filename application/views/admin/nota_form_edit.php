<div class="primarybox">
    <div class="box-header">
        <div class="box-title">
            Edit Data
        </div>
    </div>
    <div class="box box-body">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'action' => base_url('nota/nota_update/' . $model->id),
            'id' => 'fAnggaranUse',
            'enableAjaxValidation' => true,
        ));
        ?>
        <table class="table table-form">
            <tr>
                <th><?= $form->label($model, 'Pengusul'); ?></th>
                <td>
                    <?= Chosen::activeDropDownList($model, 'id_divisi', CHtml::listData(TrefDivisi::model()->findAll(), 'id', 'nama'), ['prompt' => 'Pilih Unit Pengusul']); ?>
                    <?= $form->error($model, 'id_divisi'); ?>
                </td>
            </tr>

            <tr>
                <th><?= $form->label($model, 'Tanggal'); ?></th>
                <td>
                    <?= $form->dateField($model, 'tanggal', ['class' => 'form-control']); ?>
                    <?= $form->error($model, 'tanggal'); ?>
                </td>
            </tr>

            <tr>
                <th><?= $form->label($model, 'Judul'); ?></th>
                <td>
                    <?= $form->textField($model, 'judul', ['class' => 'form-control form-control-long']) ?>
                    <?= $form->error($model, 'judul'); ?>
                </td>
            </tr>

            <tr>
                <th><?= $form->label($model, 'Nomor Nota'); ?></th>
                <td>
                    <?= $form->textField($model, 'tgl_nota', ['class' => 'form-control form-control-long']) ?>
                    <?= $form->error($model, 'tgl_nota'); ?>
                </td>
            </tr>

            <tr>
                <th><?= $form->label($model, 'Nilai Proyek'); ?></th>
                <td>
                    <?= $form->textField($model, 'nilai_proyek', ['class' => 'form-control form-control-long']) ?>
                    <?= $form->error($model, 'nilai_proyek'); ?>
                </td>
            </tr>

            <tr>
                <th><?= $form->label($model, 'Nota Justifikasi'); ?></th>
                <td>
                    <a data-fancybox="iframe" class="btn btn-sm btn-link" href="<?= asset_url('files/nota/' . $model->file_nota) ?>"><?= $model->file_nota ?></a>
                    <input type="file" name="TmstNota[file_nota]" class="form-control">
                </td>
            </tr>

            <tr>
                <th>File Pendukung</th>
                    <td>
                        <div id="keterangan">
                            <?php if ($modelPendukung) {
                                $urutan = 0; ?>
                                <?php foreach ($modelPendukung as $key => $value) {
                                    $urutan++; ?>
                                    <div id="keterangan_hapus_<?= $urutan ?>">
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <?= $urutan ?>.&nbsp;
                                                <a data-fancybox="iframe" class="btn btn-sm btn-link" href="<?= asset_url('files/pendukung/' . $value->file_pendukung) ?>"><?= $value->file_pendukung ?></a>
                                            </div>
                                            <div class="col-xs-1">
                                                <button type="button" class="pull-right btn btn-sm btn-default" onclick="hapusPendukung(<?= $value->id ?>,<?= $urutan ?>)">
                                                    <i class="icon-cross"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                <?php } ?>
                            <?php } ?>

                            <div class="row">
                                <div class="col-xs-8">
                                    <input type="file" name="file_pendukung[]" class="form-control form-control-long">
                                </div>
                                <div class="col-xs-1">
                                    <button type="button" class="pull-right btn btn-sm btn-default" id="btn_tambah">
                                        <i class="icon-add"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </td>
            </tr>

            <tr>
                <th></th>
                <td>
                    <?= CHtml::submitButton('Simpan', ['class' => 'btn btn-sm btn-primary']); ?>
                    <a href="<?= base_url('nota/index') ?>" class="btn btn-sm btn-default"> Kembali </a>
                </td>
            </tr>
        </table>
        <?php $this->endWidget(); ?>
    </div>
</div>
<?= widgetConfrimAssets() ?>
<script>
    var urutan = 1;
    $('#btn_tambah').click(function(e) {
        e.preventDefault()
        urutan++;

        $('#keterangan').append('\
        <div class="row" id="keterangan_list_' + urutan + '" style="margin-top:5px">\
            <div class="col-xs-8">\
                <input type="file" name="file_pendukung[]" class="form-control form-control-long">\
            </div>\
            <div class="col-xs-1">\
                <button type="button" class="pull-right btn btn-sm btn-default" onclick="hapus_keterangan(this);return false;" data-id="' + urutan + '">\
                    <i class="icon-cross"></i>\
                </button>\
            </div>\
        </div>\
        ');
    });

    function hapusPendukung(id, urutan) {
        $.confirm({
            buttons: {
                confirm: function() {
                    $.post(baseURL + 'pengusul/delete_file_pendukung/' + id, function(data) {
                        notify(data)
                        $('#keterangan_hapus_' + urutan).remove();
                    })
                },
                cancel: function() {},
            }
        });

    }

    function hapus_keterangan(e) {
        var id = e.getAttribute('data-id')
        $('#keterangan_list_' + id).remove()
    }

    $('#fAnggaranUse').ajaxForm({
        success: function(responseText, statusText, xhr, $form) {
            $.notify(responseText, {
                position: 'top center',
                className: 'success',
            });
        },
        error: function(responseText, statusText, xhr, $form) {
            $.alert(xhr);
        }
    });
</script>

<script type="text/javascript">
    $('#fForm').ajaxForm(optionsAjaxForm)
</script>