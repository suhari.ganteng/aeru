<div class="box-primary">
    <div class="box-header">
        <div class="box-title">
            Tambah Monitoring
        </div>
    </div>
    <div class="box box-body">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'action' => base_url('nota/mon_save/' . $model->id),
            'id' => 'fAnggaranUse',
            'enableAjaxValidation' => true,
        )); 
        ?>
        <table class="table table-form">

            <tr>
                <th>Profile Risiko</th>
                <td>
                <?php $this->widget('SummernoteWidget', ['model' => $model, 'attribute' => 'pro_risk', 'id' => 'summernoteModal', 'clientOptions' => summernoteOptions(), 'plugins' => []]); ?>
                </td>
            </tr>

            <tr>
                <th>Rencana Mitigasi</th>
                <td>
                    <?php $this->widget('SummernoteWidget', ['model' => $model, 'attribute' => 'mitigasi', 'id' => 'summernoteModal', 'clientOptions' => summernoteOptions(), 'plugins' => []]); ?>
                </td>
            </tr>

            <tr>
                <th>Monitoring Risiko</th>
                <td>
                <?php $this->widget('SummernoteWidget', ['model' => $model, 'attribute' => 'monitoring', 'id' => 'summernoteModal', 'clientOptions' => summernoteOptions(), 'plugins' => []]); ?>
                </td>
            </tr>

            <tr>
                <th>Progress Proyek</th>
                <td>
                    <?php $this->widget('SummernoteWidget', ['model' => $model, 'attribute' => 'proyek', 'id' => 'summernoteModal', 'clientOptions' => summernoteOptions(), 'plugins' => []]); ?>
                </td>
            </tr>

            <tr>
                <th></th>
                <td>
                    <?= CHtml::submitButton('Simpan', ['class' => 'btn btn-sm btn-primary']); ?>
                    <a href="<?= base_url('') ?>" class="btn btn-sm btn-default"> Kembali </a>
                </td>
            </tr>
        </table>
        <?php $this->endWidget(); ?>
    </div>
</div>
<?= widgetConfrimAssets() ?>

<script>
    var urutan = 1;
    $('#btn_tambah').click(function(e) {
        e.preventDefault()
        urutan++;

        $('#keterangan').append('\
        <div class="row" id="keterangan_list_' + urutan + '" style="margin-top:5px">\
            <div class="col-xs-8">\
                <input type="file" name="file_pendukung[]" class="form-control form-control-long">\
            </div>\
            <div class="col-xs-1">\
                <button type="button" class="pull-right btn btn-sm btn-default" onclick="hapus_keterangan(this);return false;" data-id="' + urutan + '">\
                    <i class="icon-cross"></i>\
                </button>\
            </div>\
        </div>\
        ');
    });

    $('#fAnggaranUse').ajaxForm({
        success: function(responseText, statusText, xhr, $form) {
            $.notify(responseText, {
                position: 'top center',
                className: 'success',
            });
        },
        error: function(responseText, statusText, xhr, $form) {
            $.alert(xhr);
        }
    });
</script>

<script type="text/javascript">
    $('#fForm').ajaxForm(optionsAjaxForm)
</script>