<div class="box box-primary">
    <div class="box-header">
        <div class="box-title">
            Input Data Baru
        </div>
    </div>

    <div class="box-body">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'action' => base_url('nota/nota_save'),
            'id' => 'fAnggaranUse',
            'enableAjaxValidation' => true,
        ));
        ?>
        <table class="table table-form">
            <tr>
                <th>Pengusul</th>
                <td>
                    <?= Chosen::activeDropDownList($model, 'id_divisi', CHtml::listData(TrefDivisi::model()->findAll(), 'id', 'nama'), ['prompt' => 'Pilih Unit Pengusul']); ?>
                    <?= $form->error($model, 'id_divisi'); ?>
                </td>
            </tr>
            <tr>
                <th>Tanggal</th>
                <td>
                    <?= $form->dateField($model, 'tanggal', ['class' => 'form-control']) ?>
                </td>
            </tr>

            <tr>
                <th>Judul</th>
                <td>
                    <?= $form->textField($model, 'judul', ['class' => 'form-control']) ?>
                </td>
            </tr>

            <tr>
                <th>Nomor Nota</th>
                <td>
                    <?= $form->textField($model, 'tgl_nota', ['class' => 'form-control']) ?>
                </td>
            </tr>

            <tr>
                <th>Nilai Proyek</th>
                <td>
                    <?= $form->textField($model, 'nilai_proyek', ['class' => 'form-control']) ?>
                </td>
            </tr>

            <tr>
                <th>File Nota</th>
                <td>
                    <input type="file" name="TmstNota[file_nota]" class="form-control">
                </td>
            </tr>

            <tr>
                <th>File Pendukung</th>
                <td>
                    <div id="keterangan">
                        <div class="row keterangan_list">
                            <div class="col-xs-8">
                                <input type="file" name="file_pendukung[]" class="form-control form-control-long">
                            </div>
                            <div class="col-xs-1">
                                <button type="button" class="pull-right btn btn-sm btn-default" id="btn_tambah">
                                    <i class="icon-add"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" class="btn btn-sm btn-primary btn-simpan" value=" Simpan ">
                    <a href="<?= base_url('nota/index') ?>" class="btn btn-sm btn-default"> Kembali </a>
                </td>
            </tr>

        </table>
        <?php $this->endWidget(); ?>

    </div>
</div>


<script>
    var urutan = 1;
    $('#btn_tambah').click(function(e) {
        e.preventDefault()
        urutan++;

        $('#keterangan').append('\
        <div class="row keterangan_list" id="keterangan_list_' + urutan + '" style="margin-top:5px">\
            <div class="col-xs-8">\
                <input type="file" name="file_pendukung[]" class="form-control form-control-long">\
            </div>\
            <div class="col-xs-1">\
                <button type="button" class="pull-right btn btn-sm btn-default" onclick="hapus_keterangan(this);return false;" data-id="' + urutan + '">\
                    <i class="icon-cross"></i>\
                </button>\
            </div>\
        </div>\
        ');
    });

    function hapus_keterangan(e) {
        var id = e.getAttribute('data-id')
        $('#keterangan_list_' + id).remove()
    }

    $('#fAnggaranUse').ajaxForm(optionsAjaxForm)
</script>