<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Monitoring Profile Risiko</h3>
        <!-- <div class="box-tools">
            <a href="<?= base_url('nota/addmo_form') ?>" class="btn btn-sm btn-primary"><i class="icon-plus3 right"></i>Tambah</a>
        </div> -->
    </div>
    <div class="box-body">
        <?php
        $this->load->widget('UGridView', [
            'columns' => [
                [
                    'header' => 'No',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                ],
                [
                    'header' => 'Judul Nota Justifikasi',
                    'name' => 'judul',
                    'value' => '$data->judul',
                    'type' => 'raw',
                ],
                [
                    'header' => 'Nomor Nota Justifikasi',
                    'name' => 'tgl_nota',                 
                    'value' => '$data->tgl_nota',
                    'type' => 'raw',
                ],
                [
                    'header' =>'Profile Risiko',
                    'name'  =>'pro_risk',
                    'value' => '$data->pro_risk',
                    'type' => 'raw',
                ],
                [   
                    'header' => 'Rencana Mitigasi',
                    'name' => 'mitigasi',
                    'value' => '$data->mitigasi',
                    'type' => 'raw',
                ],
                [   
                    'header' => 'Progres Proyek',
                    'name' => 'proyek',
                    'value' => '$data->proyek',
                    'type' => 'raw',
                ],
                [
                    'header' =>'Monitoring Risiko',
                    'name' => 'monitoring',
                    'value' => '$data->monitoring',
                    'type' => 'raw',
                ],
            ],
            'dataProvider'      => $model->search(),
            'filter'            => $model,
            'itemsCssClass'     => 'table table-bordered table-striped',
            'afterAjaxUpdate'   => 'js:function(id,data){$.bind_crud()}',
            'id'                => 'gridKelas',
            'selectableRows'    => 2,
        ]);
        ?>
    </div>
</div>

<?= widgetConfrimAssets() ?>
<div id="modalForm" class="white-popup mfp-with-anim mfp-hide"></div>
<script>
    $(function() {
        $.bind_crud = function() {
            $('.fan_update').each(function(index) {
                var id = $(this).attr('href');
                $(this).bind('click', function() {
                    $.ajax({
                        type: "POST",
                        url: "<?= Yii::app()->request->baseUrl ?>/nota/addmo_form/" + id,
                        success: function(data) {
                            $('#modalForm').html(data);
                            $.magnificPopup.open(optionsMagnific('#modalForm', 'gridKelas'));
                        }
                    });
                    return false;
                });
            });

            $('.fan_delete').each(function(index) {
                var id = $(this).attr('href');
                $(this).bind('click', function() {
                    $.confirm({
                        title: 'Konfirmasi !',
                        content: 'Hapus data ?',
                        buttons: {
                            confirm: function() {
                                $.get("<?= Yii::app()->request->baseUrl ?>/nota/delete/" + id, function(e) {
                                    notify(e);
                                    $.magnificPopup.close();
                                    $.fn.yiiGridView.update('gridKelas');
                                });
                            },
                            cancel: function() {},
                        }
                    });
                    return false;
                });
            });
        }
         $.bind_crud();
    });

</script>
