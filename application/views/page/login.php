<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AERU - LOGIN</title>
    <link rel="icon" href="<?php echo asset_url('public/images/icon.png'); ?>" type="image/gif">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <?= set_css('public/plugins/bootstrap/css/bootstrap.min.css') ?>
    <?= set_css('public/adminlte.min.css') ?>
    <?= set_css('public/icons/fontawesome/styles.min.css') ?>
    <?= set_css('public/plugins/sweetalert2/sweetalert2.min.css') ?>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style type="text/css">
        .btn-loading:before {
            position: absolute;
            content: '';
            top: 50%;
            left: 50%;
            margin: -.64285714em 0 0 -.64285714em;
            width: 1.28571429em;
            height: 1.28571429em;
            border-radius: 500rem;
            border: .2em solid rgba(0, 0, 0, .15);
        }

        .btn-loading:after {
            position: absolute;
            content: '';
            top: 50%;
            left: 50%;
            margin: -.64285714em 0 0 -.64285714em;
            width: 1.28571429em;
            height: 1.28571429em;
            -webkit-animation: button-spin .3s linear;
            animation: button-spin .3s linear;
            -webkit-animation-iteration-count: infinite;
            animation-iteration-count: infinite;
            border-radius: 500rem;
            border-color: #fff transparent transparent;
            border-style: solid;
            border-width: .2em;
            -webkit-box-shadow: 0 0 0 1px transparent;
            box-shadow: 0 0 0 1px transparent;
        }

        .btn-loading {
            position: relative;
            cursor: wait;
            text-shadow: none !important;
            color: transparent !important;
            opacity: 1;
            pointer-events: none;
            -webkit-transition: all 0s linear, opacity .1s ease;
            transition: all 0s linear, opacity .1s ease;
        }

        @-webkit-keyframes button-spin {
            from {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            to {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes button-spin {
            from {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            to {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
    </style>
    <style type="text/css">
        body.swal2-height-auto {
            height: 100vh !important;
        }

        .login-page,
        .register-page {
            background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:svgjs='http://svgjs.com/svgjs' width='200' height='200' preserveAspectRatio='none' viewBox='0 0 200 200'%3e%3cg mask='url(%26quot%3b%23SvgjsMask1098%26quot%3b)' fill='none'%3e%3crect width='200' height='200' x='0' y='0' fill='%230e2a47'%3e%3c/rect%3e%3cuse xlink:href='%23SvgjsPath1099' x='30' y='30' stroke='%231c538e'%3e%3c/use%3e%3cuse xlink:href='%23SvgjsPath1100' x='30' y='90' stroke='%231c538e'%3e%3c/use%3e%3cuse xlink:href='%23SvgjsPath1101' x='30' y='150' stroke='%231c538e'%3e%3c/use%3e%3cuse xlink:href='%23SvgjsPath1102' x='30' y='210' stroke='%231c538e'%3e%3c/use%3e%3cuse xlink:href='%23SvgjsPath1103' x='90' y='30' stroke='%231c538e' stroke-width='3'%3e%3c/use%3e%3cuse xlink:href='%23SvgjsPath1102' x='90' y='90' stroke='%231c538e'%3e%3c/use%3e%3cuse xlink:href='%23SvgjsPath1101' x='90' y='150' stroke='%231c538e'%3e%3c/use%3e%3cuse xlink:href='%23SvgjsPath1104' x='90' y='210' stroke='%231c538e'%3e%3c/use%3e%3cuse xlink:href='%23SvgjsPath1104' x='150' y='30' stroke='%231c538e'%3e%3c/use%3e%3cuse xlink:href='%23SvgjsPath1101' x='150' y='90' stroke='%231c538e'%3e%3c/use%3e%3cuse xlink:href='%23SvgjsPath1104' x='150' y='150' stroke='%231c538e'%3e%3c/use%3e%3cuse xlink:href='%23SvgjsPath1102' x='150' y='210' stroke='%231c538e'%3e%3c/use%3e%3cuse xlink:href='%23SvgjsPath1100' x='210' y='30' stroke='%231c538e'%3e%3c/use%3e%3cuse xlink:href='%23SvgjsPath1100' x='210' y='90' stroke='%231c538e'%3e%3c/use%3e%3cuse xlink:href='%23SvgjsPath1101' x='210' y='150' stroke='%231c538e'%3e%3c/use%3e%3cuse xlink:href='%23SvgjsPath1100' x='210' y='210' stroke='%231c538e'%3e%3c/use%3e%3c/g%3e%3cdefs%3e%3cmask id='SvgjsMask1098'%3e%3crect width='200' height='200' fill='white'%3e%3c/rect%3e%3c/mask%3e%3cpath d='M-1 0 a1 1 0 1 0 2 0 a1 1 0 1 0 -2 0z' id='SvgjsPath1099'%3e%3c/path%3e%3cpath d='M-3 0 a3 3 0 1 0 6 0 a3 3 0 1 0 -6 0z' id='SvgjsPath1100'%3e%3c/path%3e%3cpath d='M-5 0 a5 5 0 1 0 10 0 a5 5 0 1 0 -10 0z' id='SvgjsPath1101'%3e%3c/path%3e%3cpath d='M2 -2 L-2 2z' id='SvgjsPath1102'%3e%3c/path%3e%3cpath d='M6 -6 L-6 6z' id='SvgjsPath1104'%3e%3c/path%3e%3cpath d='M30 -30 L-30 30z' id='SvgjsPath1103'%3e%3c/path%3e%3c/defs%3e%3c/svg%3e");
        }
    </style>
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-box-body">
            <center>
                <img src="<?= base_url('assets/public/images/logo peruri.png') ?>" alt="Logo" style="width:50%">
            </center>
            <br>
            <!-- <p class="login-box-msg">Sign in to start your session</p>  -->
            <form id="frm_Login">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Username" name="email" required="">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Password" name="password" required="">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <button type="submit" class="btn btn-primary btn-block btn-flat" id="btn_login">Sign In</button>
            </form>
            <hr>
            <pre style="text-align: center;">&copy; Analisis & Evaluasi Risiko Usaha</pre>
        </div>
    </div>
</body>

<?= set_js('public/plugins/jquery/jquery.min.js') ?>
<?= set_js('public/plugins/bootstrap/js/bootstrap.bundle.min.js') ?>
<?= set_js('public/plugins/sweetalert2/sweetalert2.min.js') ?>
<?= set_js('public/adminlte.min.js') ?>

<script type="text/javascript">
    var baseURL = "<?php echo base_url() ?>";
    $("#frm_Login").submit(function(e) {
        e.preventDefault();
        var email = $("[name=email]").val();
        var password = $("[name=password]").val();
        var btn = $("#btn_login");
        btn.addClass('btn-loading')

        $.post(baseURL + 'auth/processLogin', {
            email: email,
            password: password
        }, function(result) {
            btn.removeClass('btn-loading')
            var user = JSON.parse(result);
            if (user['status'] == 'success') {
                Swal.fire('Login success !', 'Please wait ...', 'success')
                setTimeout(function() {
                    window.location = baseURL + 'apps';
                }, 600);
            } else {
                Swal.fire('Error !', user['msg'], 'error')
            }
        })
    })
</script>

</html>