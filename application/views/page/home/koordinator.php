<h4> Home </h4>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Dosen</span>
                <span class="info-box-number">
                    <?php echo TranPenggunaSistem::model()->with(['idRole' => ['alias' => 'tr']])->count(['condition' => "tr.nama_singkat = 'dosen' AND t.id_institusi = " . $this->session->userdata('user_institution_id')]); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-purple"><i class="fa fa-book"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Mata Kuliah</span>
                <span class="info-box-number">
                    <?php echo  TranMataKuliahKurikulum::model()->with(['idKurikulum' => ['alias' => 'tkur']])->count(['condition' => "tkur.id_institusi = " . $this->session->userdata('user_institution_id')]); ?>
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-flask"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Dosen Matakuliah</span>
                <span class="info-box-number">
                    <?php echo  TranKelas::model()->with(['idMataKuliahKurikulum.idKurikulum' => ['alias' => 'tkur']])->count(['condition' => "tkur.id_institusi = " . $this->session->userdata('user_institution_id')]); ?>
                </span>
            </div>
        </div>
    </div>
</div>