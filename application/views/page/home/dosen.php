<h4>RPS</h4>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="box box-body box-flex">
            <div class="box-flex-icon">
                <span class="bg-blue">
                    <i class="icon-stack-check"></i>
                </span>
            </div>
            <div class="box-flex-text">
                <b class="text-muted">Belum diisi</b>
                <div class="text-bold">
                    <?= TranPengajarKelas::model()->with([
                        'idPengajar' => ['alias' => 'tps'],
                        'idPengajar.idInstitusi' => [
                            'alias' => 'ti'
                        ],
                        'idKelas' => ['alias' => 'tk']
                    ])->count([
                        'condition' => "tps.id_pengguna = " . $this->session->userdata('user_id') . " AND tk.id_periode_akademik = " . $this->session->userdata('user_institution_periode_id') . " AND status_rps = 'kosong'"
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="box box-body box-flex">
            <div class="box-flex-icon">
                <span class="bg-blue">
                    <i class="icon-stack-check"></i>
                </span>
            </div>
            <div class="box-flex-text">
                <b class="text-muted">Menunggu Pemeriksaan</b>
                <div class="text-bold">
                    <?= TranPengajarKelas::model()->with([
                        'idPengajar' => ['alias' => 'tps'],
                        'idPengajar.idInstitusi' => [
                            'alias' => 'ti'
                        ],
                        'idKelas' => ['alias' => 'tk']
                    ])->count([
                        'condition' => "tps.id_pengguna = " . $this->session->userdata('user_id') . " AND tk.id_periode_akademik = " . $this->session->userdata('user_institution_periode_id') . " AND status_rps = 'menunggu'"
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="box box-body box-flex">
            <div class="box-flex-icon">
                <span class="bg-blue">
                    <i class="icon-stack-check"></i>
                </span>
            </div>
            <div class="box-flex-text">
                <b class="text-muted">Revisi</b>
                <div class="text-bold">
                    <?= TranPengajarKelas::model()->with([
                        'idPengajar' => ['alias' => 'tps'],
                        'idPengajar.idInstitusi' => [
                            'alias' => 'ti'
                        ],
                        'idKelas' => ['alias' => 'tk']
                    ])->count([
                        'condition' => "tps.id_pengguna = " . $this->session->userdata('user_id') . " AND tk.id_periode_akademik = " . $this->session->userdata('user_institution_periode_id') . " AND status_rps = 'revisi'"
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="box box-body box-flex">
            <div class="box-flex-icon">
                <span class="bg-blue">
                    <i class="icon-stack-check"></i>
                </span>
            </div>
            <div class="box-flex-text">
                <b class="text-muted">Selesai</b>
                <div class="text-bold">
                    <?= TranPengajarKelas::model()->with([
                        'idPengajar' => ['alias' => 'tps'],
                        'idPengajar.idInstitusi' => [
                            'alias' => 'ti'
                        ],
                        'idKelas' => ['alias' => 'tk']
                    ])->count([
                        'condition' => "tps.id_pengguna = " . $this->session->userdata('user_id') . " AND tk.id_periode_akademik = " . $this->session->userdata('user_institution_periode_id') . " AND status_rps = 'selesai'"
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<h4>Kontrak Kuliah</h4>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="box box-body box-flex">
            <div class="box-flex-icon">
                <span class="bg-green">
                    <i class="icon-stack-check"></i>
                </span>
            </div>
            <div class="box-flex-text">
                <b class="text-muted">Belum diisi</b>
                <div class="text-bold">
                    <?= TranPengajarKelas::model()->with([
                        'idPengajar' => ['alias' => 'tps'],
                        'idPengajar.idInstitusi' => [
                            'alias' => 'ti'
                        ],
                        'idKelas' => ['alias' => 'tk']
                    ])->count([
                        'condition' => "tps.id_pengguna = " . $this->session->userdata('user_id') . " AND tk.id_periode_akademik = " . $this->session->userdata('user_institution_periode_id') . " AND status_kontrak = 'kosong'"
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="box box-body box-flex">
            <div class="box-flex-icon">
                <span class="bg-green">
                    <i class="icon-stack-check"></i>
                </span>
            </div>
            <div class="box-flex-text">
                <b class="text-muted">Menunggu Pemeriksaan</b>
                <div class="text-bold">
                    <?= TranPengajarKelas::model()->with([
                        'idPengajar' => ['alias' => 'tps'],
                        'idPengajar.idInstitusi' => [
                            'alias' => 'ti'
                        ],
                        'idKelas' => ['alias' => 'tk']
                    ])->count([
                        'condition' => "tps.id_pengguna = " . $this->session->userdata('user_id') . " AND tk.id_periode_akademik = " . $this->session->userdata('user_institution_periode_id') . " AND status_kontrak = 'menunggu'"
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="box box-body box-flex">
            <div class="box-flex-icon">
                <span class="bg-green">
                    <i class="icon-stack-check"></i>
                </span>
            </div>
            <div class="box-flex-text">
                <b class="text-muted">Revisi</b>
                <div class="text-bold">
                    <?= TranPengajarKelas::model()->with([
                        'idPengajar' => ['alias' => 'tps'],
                        'idPengajar.idInstitusi' => [
                            'alias' => 'ti'
                        ],
                        'idKelas' => ['alias' => 'tk']
                    ])->count([
                        'condition' => "tps.id_pengguna = " . $this->session->userdata('user_id') . " AND tk.id_periode_akademik = " . $this->session->userdata('user_institution_periode_id') . " AND status_kontrak = 'revisi'"
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="box box-body box-flex">
            <div class="box-flex-icon">
                <span class="bg-green">
                    <i class="icon-stack-check"></i>
                </span>
            </div>
            <div class="box-flex-text">
                <b class="text-muted">Selesai</b>
                <div class="text-bold">
                    <?= TranPengajarKelas::model()->with([
                        'idPengajar' => ['alias' => 'tps'],
                        'idPengajar.idInstitusi' => [
                            'alias' => 'ti'
                        ],
                        'idKelas' => ['alias' => 'tk']
                    ])->count([
                        'condition' => "tps.id_pengguna = " . $this->session->userdata('user_id') . " AND tk.id_periode_akademik = " . $this->session->userdata('user_institution_periode_id') . " AND status_kontrak = 'selesai'"
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>