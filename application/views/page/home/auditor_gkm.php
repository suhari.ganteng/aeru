<h4> Home </h4>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="box box-body box-flex">
            <div class="box-flex-icon">
                <span class="bg-blue">
                    <i class="icon-stack-check"></i>
                </span>
            </div>
            <div class="box-flex-text">
                <b class="text-muted">Soal Ujian : Menunggu Pemeriksaan</b>
                <div class="text-bold">
                    <?php
                    $model = new TranPertemuanKelas();
                    $model->unsetAttributes();
                    $model->whereOfPeriode      = $this->session->userdata('user_institution_periode_id');
                    $model->whereOfInstitution  = $this->session->userdata('user_institution_id');
                    $model->wherePertemuanUjian = [8, 16];
                    $model->wherePertemuanUjian = [8, 16];
                    $model->status_soal_ujian   = 'menunggu';
                    
                    echo count($model->search()->getData());
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>