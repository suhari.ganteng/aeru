<h4> Home </h4>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="box box-body box-flex">
            <div class="box-flex-icon">
                <span class="bg-blue">
                    <i class="fa fa-exclamation-triangle"></i>
                </span>
            </div>
            <div class="box-flex-text">
                <b class="text-muted">RPS Menunggu Pemeriksaan</b>
                <div class="text-bold">
                    <?php echo  TranPengajarKelas::model()->with(['idKelas.idMataKuliahKurikulum.idKurikulum' => ['alias' => 'tkur']])->count(['condition' => "t.status_rps = 'menunggu' AND tkur.id_institusi = " . $this->session->userdata('user_institution_id')]); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="box box-body box-flex">
            <div class="box-flex-icon">
                <span class="bg-blue">
                    <i class="fa fa-exclamation-triangle"></i>
                </span>
            </div>
            <div class="box-flex-text">
                <b class="text-muted">Kontrak Kuliah Menunggu Pemeriksaan</b>
                <div class="text-bold">
                    <?php echo  TranPengajarKelas::model()->with(['idKelas.idMataKuliahKurikulum.idKurikulum' => ['alias' => 'tkur']])->count(['condition' => "t.status_kontrak = 'menunggu' AND tkur.id_institusi = " . $this->session->userdata('user_institution_id')]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="alert alert-warning">
    Dikarenakan ada Bug file AIS hilang, harap periksa kembali file AIS dikolom file AIS. <br>
    Jika ada yang hilang, rubah status RPS menjadi revisi untuk diupload ulang file AIS oleh Dosen penyusun RPS
</div>