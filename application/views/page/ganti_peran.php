<div class="row">
    <div class="col-xs-12">
        <h4>Ganti Peran</h4>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-4">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h5 class="box-title">
                    Pilih Peran
                </h5>
            </div>
            <div class="box-body">
                <form method="POST">
                    <?php
                    $options = CHtml::listData(TranPenggunaSistem::model()->findAllByAttributes(['id_pengguna' => $this->session->userdata('user_id')]), 'id', 'RoleInstitusi');
                    echo CHtml::dropDownList('id_pengguna_sistem', $this->session->userdata('user_id_peran'), $options, ['class' => 'form-control']);
                    ?>
                    <hr>
                    <div class="pull-right">
                        <input type="submit" value="Ganti Peran" class="btn btn-primary btn-sm">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>