<div class="row">
    <div class="col-xs-12 col-sm-6">
        <div class="box box-primary">
            <div class="box-header">
                <h5 class="box-title">Konfigurasi</h5>
            </div>
            <div class="box-body">
                <?php $form = $this->beginWidget('CActiveForm', ['id' => 'fAkun']);   ?>
                <table class="table table-form table-mobile">
                    <tr>
                        <th> <?php echo $form->labelEx($model, 'nama'); ?></th>
                        <td>
                            <?php echo $form->textField($model, 'nama', ['class' => 'form-control', 'disabled' => 'disabled']); ?>
                            <?php echo $form->error($model, 'nama'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th> <?php echo $form->labelEx($model, 'kaprodi'); ?></th>
                        <td>
                            <?php echo Chosen::activeDropDownList($model, 'kaprodi', CHtml::listData(TmstPengguna::model()->findAll(), 'id', 'nama'), ['prompt' => 'Pilih Kaprodi']); ?>
                            <?php echo $form->error($model, 'kaprodi'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th> <?php echo CHtml::label('Periode Aktif', ''); ?></th>
                        <td>
                            <?php echo Chosen::activeDropDownList($model, 'id_periode_akademik', CHtml::listData(TrefPeriodeAkademik::model()->findAll(), 'id', 'tahun_akademik'), ['prompt' => 'Pilih Periode Aktif']); ?>
                            <?php echo $form->error($model, 'id_periode_akademik'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>
                            <?php echo CHtml::submitButton('Simpan', ['class' => 'btn btn-sm btn-primary']); ?>
                        </td>
                    </tr>
                </table>
                <?php $this->endWidget(); ?>
                <script>
                    $('#fAkun').ajaxForm(optionsAjaxForm)
                </script>
            </div>
        </div>
    </div>

</div>