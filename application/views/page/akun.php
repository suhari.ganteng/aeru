<div class="row">
    <div class="col-xs-12 col-sm-6">
        <div class="box box-primary">
            <div class="box-header">
                <h5 class="box-title">Akun Detail</h5>
            </div>
            <div class="box-body">
                
                
                <?php $form = $this->beginWidget('CActiveForm', ['id' => 'fAkun']);   ?>
                <table class="table table-form table-mobile">
                    <tr>
                        <th> <?php echo $form->labelEx($model, 'username'); ?></th>
                        <td>
                            <?php echo $form->textField($model, 'username', ['class' => 'form-control', 'disabled' => 'disabled']); ?>
                            <?php echo $form->error($model, 'username'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th> <?php echo $form->labelEx($model, 'password'); ?></th>
                        <td>
                            <?php echo CHtml::passwordField('password', null, ['class' => 'form-control']); ?>
                            <small class="text-muted">Biarkan kosong jika password sama</small>
                        </td>
                    </tr>
                    <tr>
                        <th> <?php echo $form->labelEx($model, 'np'); ?></th>
                        <td>
                            <?php echo $form->textField($model, 'np', ['class' => 'form-control']); ?>
                            <?php echo $form->error($model, 'np'); ?>
                        </td>
                    </tr>

                    <tr>
                        <th> <?php echo $form->labelEx($model, 'nama'); ?></th>
                        <td>
                            <?php echo $form->textField($model, 'nama', ['class' => 'form-control']); ?>
                            <?php echo $form->error($model, 'nama'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>
                            <?php echo CHtml::submitButton('Simpan', ['class' => 'btn btn-sm btn-primary']); ?>
                        </td>
                    </tr>
                </table>
                <?php $this->endWidget(); ?>
                <script>
                    $('#fAkun').ajaxForm(optionsAjaxForm)
                </script>
            </div>
        </div>
    </div>
</div>