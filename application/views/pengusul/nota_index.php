<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Nota Justifikasi</h3>
    </div>
    <div class="box-body">
        <?php
        $this->load->widget('UGridView', [
            'columns' => [
                [
                    'header' => 'No',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                ],
                [
                    'header' => 'Tanggal',
                    'value' => '$data->tanggal',
                    'name' => 'tanggal',
                    'filter' => CHtml::activeDateField($model, 'tanggal'),
                ],
                [
                    'header' => 'Judul',
                    'value' => '$data->judul',
                    'name' => 'judul',
                    'filter' => CHtml::activeTextField($model, 'judul'),
                ],
                [
                    'header' => 'Nomor Nota',
                    'value' => '$data->tgl_nota',
                    'name' => 'tgl_nota',
                    'filter' => CHtml::activeTextField($model, 'tgl_nota'),
                ],
            ],
            'dataProvider'      => $model->search(),
            'filter'            => $model,
            'itemsCssClass'     => 'table table-bordered table-striped',
            'afterAjaxUpdate'   => 'js:function(id,data){$.bind_crud()}',
            'id'                => 'gridKelas',
            'selectableRows'    => 2,
        ]);
        ?>
    </div>
</div>

<?= widgetConfrimAssets() ?>
<div id="modalForm" class="white-popup mfp-with-anim mfp-hide"></div>
<script>
    $(function() {
        $.bind_crud = function() {
            $('.fan_update').each(function(index) {
                var id = $(this).attr('href');
                $(this).bind('click', function() {
                    $.ajax({
                        type: "POST",
                        url: "<?= Yii::app()->request->baseUrl ?>/nota/nota_edit/" + id,
                        success: function(data) {
                            $('#modalForm').html(data);
                            $.magnificPopup.open(optionsMagnific('#modalForm', 'gridKelas'));
                        }
                    });
                    return false;
                });
            });

            $('.fan_delete').each(function(index) {
                var id = $(this).attr('href');
                $(this).bind('click', function() {
                    $.confirm({
                        title: 'Konfirmasi !',
                        content: 'Hapus data ?',
                        buttons: {
                            confirm: function() {
                                $.get("<?= Yii::app()->request->baseUrl ?>/nota/delete/" + id, function(e) {
                                    notify(e);
                                    $.magnificPopup.close();
                                    $.fn.yiiGridView.update('gridKelas');
                                });
                            },
                            cancel: function() {},
                        }
                    });
                    return false;
                });
            });
        }
         $.bind_crud();
    });

</script>