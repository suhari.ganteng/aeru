<div class="box box-primary">
    <div class="box-header">
        <div class="box-title">
            Input Data Baru
        </div>
    </div>

    <div class="box-body">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'action' => base_url('nota/add_concern'),
            'id' => 'fAnggaranUse',
            'enableAjaxValidation' => true,
        ));
        ?>
        <table class="table table-form">
            <tr>
                <th>No NJK</th>
                <td>
                    <?= $form->textField($model, 'tgl_nota', ['class' => 'form_control']); ?>
                </td>
            </tr>
            <tr>
                <th>No Review</th>
                <td>
                    <?= $form->textField($model, 'no_review', ['class' => 'form_control']); ?>
                </td>
            </tr>
            <tr>
                <th>Tanggal Review</th>
                <td>
                    <?= $form->dateField($model, 'tanggal_diperiksa', ['class' => 'form_control']); ?>
                </td>
            </tr>
            <tr>
                <th>Judul Nota</th>
                <td>
                    <?= $form->textField($model, 'judul', ['class' => 'form_control']); ?>
                </td>
            </tr>
            <tr>
                <th>Divisi</th>
                <td>
                    <?= Chosen::activeDropDownList($model, 'id_divisi', CHtml::listData(TrefDivisi::model()->findAll(), 'id', 'nama'), ['prompt' => 'Pilih Unit Pengusul']); ?>
                    <?= $form->error($model, 'id_divisi'); ?>
                </td>
            </tr>
            <tr>
                <th>Concern</th>
                <td>
                    <?php $this->widget('SummernoteWidget', ['model' => $model, 'attribute' => 'concern', 'id' => 'summernoteModal', 'clientOptions' => summernoteOptions(), 'plugins' => []]); ?>        
                </td>
            </tr>
            <tr>
                <th>Target Waktu</th>
                <td>
                    <?= $form->textField($model, 'target_waktu', ['class' => 'form_control']); ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" class="btn btn-sm btn-primary btn-simpan" value=" Simpan ">
                    <a href="<?= base_url('nota/index') ?>" class="btn btn-sm btn-default"> Kembali </a>
                </td>
            </tr>

        </table>
        <?php $this->endWidget(); ?>

    </div>
</div>


<script>
    var urutan = 1;
    $('#btn_tambah').click(function(e) {
        e.preventDefault()
        urutan++;

        $('#keterangan').append('\
        <div class="row keterangan_list" id="keterangan_list_' + urutan + '" style="margin-top:5px">\
            <div class="col-xs-8">\
                <input type="file" name="file_pendukung[]" class="form-control form-control-long">\
            </div>\
            <div class="col-xs-1">\
                <button type="button" class="pull-right btn btn-sm btn-default" onclick="hapus_keterangan(this);return false;" data-id="' + urutan + '">\
                    <i class="icon-cross"></i>\
                </button>\
            </div>\
        </div>\
        ');
    });

    function hapus_keterangan(e) {
        var id = e.getAttribute('data-id')
        $('#keterangan_list_' + id).remove()
    }

    $('#fAnggaranUse').ajaxForm(optionsAjaxForm)
</script>