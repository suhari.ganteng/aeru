<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Monitoring Profile Risiko</h3>
    </div>
    <div class="box-body">
        <?php
        $this->load->widget('UGridView', [
            'columns' => [
                [
                    'header' => 'No',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                ],
                [
                    'header' => 'Judul Nota Justifikasi',
                    'name' => 'judul',
                    'value' => '$data->judul',
                    'type' => 'raw',
                ],
                [
                    'header' => 'Nomor Nota Justifikasi',
                    'name' => 'tgl_nota',                 
                    'value' => '$data->tgl_nota',
                    'type' => 'raw',
                ],
                [
                    'header' =>'Profile Risiko',
                    'name'  =>'pro_risk',
                    'value' => '$data->pro_risk',
                    'type' => 'raw',
                ],
                [   
                    'header' => 'Rencana Mitigasi',
                    'value' => '$data->mitigasi',
                    'name' => 'mitigasi',
                    'type' => 'raw',
                ],
                [
                    'header' =>'Monitoring',
                    'value' => '$data->monitoring',
                    'name' => 'monitoring',
                    'type' => 'raw',
                ],
                [   
                    'header' => 'Progres Proyek',
                    'value' => '$data->proyek',
                    'name' => 'proyek',
                    'type' => 'raw',
                ],
                [
                    'class' => 'CButtonColumn',
                    'buttons' => array(
                        'tran-update' => array(
                            'label' => '<button class="btn btn-xs"><i class="icon-pencil"></i>  </button>',
                            'url' => '$data->id',
                            'options' => array("class" => "fan_update", 'title' => Yii::t('admin_tran-kelas', 'Edit')),
                            
                        ),
                    ),
                    'template' => '{tran-update}',
                ],
            ],
            'dataProvider'      => $model->search(),
            'filter'            => $model,
            'itemsCssClass'     => 'table table-bordered table-striped',
            'afterAjaxUpdate'   => 'js:function(id,data){$.bind_crud()}',
            'id'                => 'gridKelas',
            'selectableRows'    => 2,
        ]);
        ?>
    </div>
</div>

<?= widgetConfrimAssets() ?>
<div id="modalForm" class="white-popup mfp-with-anim mfp-hide"></div>
<script>
    $(function() {
        $.bind_crud = function() {
            $('.fan_update').each(function(index) {
                var id = $(this).attr('href');
                $(this).bind('click', function() {
                    $.ajax({
                        type: "POST",
                        url: "<?= Yii::app()->request->baseUrl ?>/pengusul/addmo_form/" + id,
                        success: function(data) {
                            $('#modalForm').html(data);
                            $.magnificPopup.open(optionsMagnific('#modalForm', 'gridKelas'));
                        }
                    });
                    return false;
                });
            });

            $('.fan_delete').each(function(index) {
                var id = $(this).attr('href');
                $(this).bind('click', function() {
                    $.confirm({
                        title: 'Konfirmasi !',
                        content: 'Hapus data ?',
                        buttons: {
                            confirm: function() {
                                $.get("<?= Yii::app()->request->baseUrl ?>/nota/delete/" + id, function(e) {
                                    notify(e);
                                    $.magnificPopup.close();
                                    $.fn.yiiGridView.update('gridKelas');
                                });
                            },
                            cancel: function() {},
                        }
                    });
                    return false;
                });
            });
        }
         $.bind_crud();
    });

</script>