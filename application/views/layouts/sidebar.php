<section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">
            Main navigation
        </li>
        <li>
            <a href="<?= base_url('apps') ?>">
                <i class="icon-home4 right"></i>
                <span>Home</span>
            </a>
        </li>

        <?php
        switch ($this->session->userdata('user_role_name')) {
            case 'admin':
                include('sidebar/_admin.php');
                break;
            case 'pengusul':
                include('sidebar/_pengusul.php');
                break;
        }
        ?>

        <li class="header">
            Akun
        </li>
        <li>
            <a href="<?= base_url('apps/akun') ?>">
                <i class="icon-arrow-right22 right"></i>
                <span>Akun</span>
            </a>
        </li>
        <li>
            <a href="<?= base_url('apps/logout') ?>">
                <i class="icon-arrow-right22 right"></i>
                <span>Keluar</span>
            </a>
        </li>
    </ul>
</section>