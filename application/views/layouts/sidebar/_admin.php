<li>
    <a href="<?= base_url('nota/index') ?>">
        <i class="icon-arrow-right22 right"></i>
        <span>
            Nota Justifikasi
        </span>
        <!-- <span class="pull-right-container">
            <span class="label label-info pull-right">
                <?= TmstNota::model()->count() ?>
            </span>
        </span> -->
    </a>
</li>

<li>
    <a href="<?= base_url('nota/concern') ?>">
        <i class="icon-arrow-right22 right"></i>
        <span>
            Concern Risiko
        </span>
        <!-- <span class="pull-right-container">
            <span class="label label-info pull-right">
                <?= TmstNota::model()->count() ?>
            </span>
        </span> -->
    </a>
</li>

<li>
    <a href="<?= base_url('nota/review') ?>">
        <i class="icon-arrow-right22 right"></i>
        <span>
            Monitoring Risiko
        </span>
        <!-- <span class="pull-right-container">
            <span class="label label-info pull-right">
                <?= TmstNota::model()->count() ?>
            </span>
        </span> -->
    </a>
</li>

<li class="header">
    Master
</li>
<li>
    <a href="<?= base_url('master/pengguna/index/') ?>">
        <i class="icon-arrow-right22 right"></i>
        <span>
            Pengguna
        </span>
    </a>
</li>

<li>
    <a href="<?= base_url('master/pengguna_sistem/index/') ?>">
        <i class="icon-arrow-right22 right"></i>
        <span>
            Pengguna Sistem
        </span>
    </a>
</li>
<li>
    <a href="<?= base_url('master/divisi/index/') ?>">
        <i class="icon-arrow-right22 right"></i>
        <span>
            Pengusul
        </span>
    </a>
</li>