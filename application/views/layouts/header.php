<!DOCTYPE html>
<html>
<head>
    <?= set_js('public/plugins/jquery/jquery.min.js') ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" href="<?php echo asset_url('public/images/icon.png'); ?>" type="image/gif">
    <title>AERU | Risk Management</title>
    <link href="https://fonts.googleapis.com/css2?family=Fira+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&display=swap" rel="stylesheet">

    <?= set_css('public/plugins/pace/pace.css') ?>
    <?= set_js('public/plugins/pace/pace.js') ?>

    <?= set_css('public/plugins/bootstrap/css/bootstrap.min.css') ?>
    <?= set_css('public/adminlte.min.css') ?>
    <?= set_css('public/_all-skins.min.css') ?>

    <?= set_css('public/icons/fontawesome/styles.min.css') ?>
    <?= set_css('public/icons/icomoon/styles.min.css') ?>

    <?= set_js('public/plugins/bootstrap/js/bootstrap.min.js') ?>
    <?= set_js('public/adminlte.min.js') ?>
    <?= set_js('public/plugins/jquery.ba-bbq.js') ?>
    <?= set_js('public/plugins/jquery.slimscroll.min.js') ?>

    <?= set_css('public/plugins/magnific/magnific.css') ?>
    <?= set_js('public/plugins/magnific/magnific.js') ?>

    <?= set_js('public/plugins/jquery-form/jquery.form.min.js') ?>

    <?= set_css('public/plugins/jquery-ui-bootstrap/css/custom-theme/jquery-ui-1.10.0.custom.css') ?>
    <?= set_js('public/plugins/jquery-ui/jquery-ui.min.js') ?>

    <?= set_js('public/plugins/notify.js') ?>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>

    <?php if (isset($css_files)) {
        foreach ($css_files as $file) {
            echo '<link type="text/css" rel="stylesheet" href="' . $file . '" />';
        }
    }
    if (isset($js_files)) {
        foreach ($js_files as $file) {
            echo '<script src="' . $file . '"></script>';
        }
    } ?>

    <?= set_css('public/_costum.css') ?>
    <style>
        .dropdown-menu {
            right: 0;
            left: auto;
        }

        .box-flex {
            display: flex;
            flex-wrap: wrap;
        }

        .box-flex-icon {
            flex: 0 0 auto;
            width: auto;
            align-self: center;
        }

        .box-flex-icon span {
            padding: 10px;
            border-radius: 4px;
        }

        .box-flex-icon i {
            font-size: 20px;
        }

        .box-flex-text {
            flex: 1 0 0%;
            padding: 0 10px;
        }

        .table-bordered>thead>tr>th,
        .table-bordered>tbody>tr>th,
        .table-bordered>tfoot>tr>th,
        .table-bordered>thead>tr>td,
        .table-bordered>tbody>tr>td,
        .table-bordered>tfoot>tr>td {
            border: 1px solid #ddd;   
        }

        .table-bordered>thead>tr>th{
            text-align:center;
        }

        .table-bordered>thead>tr>th>a{
            color: black;
        }


        .table-bordered>caption+thead>tr:first-child>td,
        .table-bordered>caption+thead>tr:first-child>th,
        .table-bordered>colgroup+thead>tr:first-child>td,
        .table-bordered>colgroup+thead>tr:first-child>th,
        .table-bordered>thead:first-child>tr:first-child>td,
        .table-bordered>thead:first-child>tr:first-child>th {
            border-top: 1px solid #ddd;           
        }

        .table-bordered>thead>tr>th {

            font-weight: 600;
            background: #3f51b517;
        }

        .dropdown-menu>li>a {
            font-size: 12px;
            font-weight: 400;
        }

        .grid-view>.table-bordered>tbody>tr>td:first-child {
            width: 1%;
            white-space: nowrap;
        }

        .alert-warning {
            background-color: yellow !important;
            color: black !important;
        }

        input[type="radio"] {
            display: none;
        }

        input[type="text"]  {
            resize: vertical;
        }
    </style>

    <script>
        var baseURL = "<?php echo base_url() ?>";

        var optionsAjaxForm = {
            success: function(responseText, statusText, xhr, $form) {
                $.notify(responseText, {
                    position: 'top center',
                    className: 'success',
                });
            },
            error: function(responseText, statusText, xhr, $form) {
                $.alert(xhr);
            }
        };

        function notify(responseText) {
            $.notify(responseText, {
                position: 'top center',
                className: 'success',
            });
        }

        function optionsMagnific(idModal, idGridView) {
            return {
                items: {
                    src: idModal
                },
                focus: true,
                type: 'inline',
                removalDelay: 300,
                mainClass: 'mfp-move-horizontal',
                closeOnBgClick: false,
                callbacks: {
                    open: function() {
                        if ($.isFunction($.fn.summernote)) {
                            $('#summernoteModal').summernote(summernoteConfig);
                            $('.summernoteModal').summernote(summernoteConfig);
                        }


                        $(".chosen-select").chosen('destroy');
                        $(".chosen-select").chosen();

                        $.magnificPopup.instance.close = function() {
                            $.fn.yiiGridView.update(idGridView);
                            $.magnificPopup.proto.close.call(this);
                        };
                    }
                }
            }
        }

        var toolbarSummernote = [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['para', ['ul', 'ol', 'paragraph']],
            'help'
        ];

        var summernoteConfig = {
            toolbar: toolbarSummernote,
            callbacks: {
                onPaste: function(e) {
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    document.execCommand('insertText', false, bufferText);
                }
            }
        };
    </script>
</head>

<body class="fixed skin-purple-light" onload="startTime()">
    <div class="loading" style="display: none;">
        <div class="loading-image">
            <i class="icon-spinner10 spinner" style="font-size: 50px;"></i>
            <br>
            <h3> Silahkan tunggu ...</h3>
        </div>
    </div>

    <div class="wrapper">
        <header class="main-header">
            <a href="<?= base_url() ?>" class="logo">
                <span class="logo-mini">
                    <b>AERU</b>
                </span>
                <span class="logo-lg text-center" style="height: 100%;">
                    <img src="<?= base_url('assets/public/images/logo peruri.png') ?>" alt="Logo" style="height: 40px;">

                </span>
            </a>
            <nav class="navbar navbar-static-top">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <i class="fa fa-bars"></i>
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <link href='https://fonts.googleapis.com/css?family=Orbitron' rel='stylesheet' type='text/css'>
                        <li>
                            <a href="#">
                                <i class="icon-calendar52 right" style="margin-top: 0px;"></i>
                                <span style="font-family: 'Orbitron', sans-serif;letter-spacing: 2px;"> <?= date('d F Y') ?> </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon-alarm right" style="margin-top: 0px;"></i>
                                <span id="txt" style="font-family: 'Orbitron', sans-serif;letter-spacing: 2px;"></span>
                                <script>
                                    function startTime() {
                                        var today = new Date();
                                        var h = today.getHours();
                                        var m = today.getMinutes();
                                        var s = today.getSeconds();
                                        m = checkTime(m);
                                        s = checkTime(s);
                                        document.getElementById('txt').innerHTML =
                                            h + ":" + m + ":" + s;
                                        var t = setTimeout(startTime, 500);
                                    }

                                    function checkTime(i) {
                                        if (i < 10) {
                                            i = "0" + i
                                        };
                                        return i;
                                    }
                                </script>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="#">
                                <span class="label">
                                    <?= $this->session->userdata('user_divisi_name') ?>
                                </span>
                                    <?= $this->session->userdata('user_role_name') ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="main-sidebar">
            <?php include('sidebar.php') ?>
        </aside>
        <div class="content-wrapper">
            <section class="content">
                <?php if ($this->session->flashdata('success')) { ?>
                    <div class="alert alert-success" style="max-height: 300px;overflow: auto;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php } ?>

                <?php if ($this->session->flashdata('error')) { ?>
                    <div class="alert alert-danger" style="max-height: 300px;overflow: auto;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php } ?>


                <div class="hidden">
                    <?php echo Chosen::dropDownList('ucenxyz', null, []); ?>
                    <?php $this->widget('SummernoteWidget'); ?>
                </div>