<?php
$this->load->view('layouts/header');


if (isset($view)) {
    $_p = isset($params) ? $params : [];
    $this->load->view($view, $_p);
} else if (isset($output)) {
    echo $output;
}

$this->load->view('layouts/footer');
