</section>
</div>
<footer class="main-footer">
    <strong> Copyright &copy; <?= date('Y') ?> AERU</strong>
</footer>
</div>
<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover()

        if ($.isFunction($.fn.summernote)) {
            // $('#summernoteModal').summernote({
            //     toolbar: toolbarSummernote
            // });

            $('.summernoteModal').summernote({
                toolbar: toolbarSummernote,
                callbacks: {
                    onPaste: function(e) {
                        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                        e.preventDefault();
                        document.execCommand('insertText', false, bufferText);
                    }
                }
            });

            $('#summernoteModal').summernote({
                toolbar: toolbarSummernote,
                callbacks: {
                    onPaste: function(e) {
                        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                        e.preventDefault();
                        document.execCommand('insertText', false, bufferText);
                    }
                }
            });
        }

        var windowURL = window.location.href;
        var links = document.getElementsByTagName('a');
        for (var i = 0; i < links.length; i++) {
            var y = $(links[i]);
            var source = links[i].getAttribute('href');

            try {
                source = source.replace('index/', '');
                windowURL = windowURL.replace('index/', '');
                windowURL = windowURL.replace('#', '');
            } catch (e) {}

            if (windowURL.includes(source)) {
                y.parent().addClass('active');

                if (y.parent().parent().parent().hasClass('treeview')) {
                    y.parent().parent().parent().addClass('menu-open');
                }

                if (y.parent().parent().hasClass('treeview-menu')) {
                    y.parent().parent().css('display', 'block');
                }
            }
        }

        var config_nice_scroll = {
            background: "green",
            cursorwidth: "2px",
            width: "2px",
            cursorborder: "0px"
        };

        // $("html").niceScroll(config_nice_scroll);
        // $(".control-sidebar").niceScroll(config_nice_scroll);
    })

    $(document).ajaxStart(function() {
        Pace.restart()
        $('.loading').show();
    }).ajaxStop(function() {
        $('.loading').hide();
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover()
    });

    // /fix menu overflow under the responsive table 
    // hide menu on click... (This is a must because when we open a menu )
    $(document).click(function(event) {
        //hide all our dropdowns
        $('.dropdown-menu[data-parent]').hide();

    });
    $(document).on('click', '.my-scrollbar [data-toggle="dropdown"]', function() {
        // if the button is inside a modal
        if ($('body').hasClass('modal-open')) {
            throw new Error("This solution is not working inside a responsive table inside a modal, you need to find out a way to calculate the modal Z-index and add it to the element")
            return true;
        }

        $buttonGroup = $(this).parent();
        if (!$buttonGroup.attr('data-attachedUl')) {
            var ts = +new Date;
            $ul = $(this).siblings('ul');
            $ul.attr('data-parent', ts);
            $buttonGroup.attr('data-attachedUl', ts);
            $(window).resize(function() {
                $ul.css('display', 'none').data('top');
            });
        } else {
            $ul = $('[data-parent=' + $buttonGroup.attr('data-attachedUl') + ']');
        }
        if (!$buttonGroup.hasClass('open')) {
            $ul.css('display', 'none');
            return;
        }
        dropDownFixPosition($(this).parent(), $ul);

        function dropDownFixPosition(button, dropdown) {
            var dropDownTop = button.offset().top + button.outerHeight();
            dropdown.css('top', dropDownTop + "px");
            dropdown.css('left', button.offset().left + "px");
            dropdown.css('position', "absolute");

            dropdown.css('width', dropdown.width());
            dropdown.css('heigt', dropdown.height());
            dropdown.css('display', 'block');
            dropdown.css('z-index', '9999');
            dropdown.appendTo('body');
        }
    });
</script>

</body>

</html>