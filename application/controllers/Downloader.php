<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

class Downloader extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if ($this->session->userdata('isLoggedIn')) {

            if (!isset($_GET['path'])) {
                http_response_code(400);
                exit('Invalid request!');
            }

            define('AUTHORIZED_FILE_EXTENSIONS', ['pdf']);

            $path = $_GET['path'];

            $file_ext = pathinfo($path, PATHINFO_EXTENSION);
            $file_ext = strtolower($file_ext);
            if (!in_array($file_ext, AUTHORIZED_FILE_EXTENSIONS)) {
                http_response_code(401);
                exit('Not allowed!');
            }

            header("Content-Description: File Transfer");
            header('Content-type: application/pdf');
            header("Content-Disposition: inline; filename=" . basename($path));

            readfile($path);
        } else {
            http_response_code(401);
            exit('Login required!');
        }
    }
}