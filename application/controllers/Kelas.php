<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Kelas extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();
    }

    public function index()
    {
        $model = new TranKelas('search');
        $model->unsetAttributes();

        if (isset($_GET['TranKelas'])) {
            $model->attributes = $_GET['TranKelas'];

            $this->session->set_userdata('KoorTranKelas', $_GET['TranKelas']);
        } else {
            if ($this->session->userdata('KoorTranKelas') != null) {
                $model->attributes = $this->session->userdata('KoorTranKelas');
            } else {
                $model->attributes = ['periodeOfClass' => $this->session->userdata('user_institution_periode_id')];
            }
        }

        if (isset($_POST['csv'])) {

            $ps    = new Spreadsheet;
            $sheet = $ps->getActiveSheet();

            $header = array('Periode', 'Kurikulum', 'Kode MK', 'Nama MK', 'Pengajar', 'Semester', 'SKS', 'Kategori');

            foreach ($header as $key => $value) {
                $sheet->setCellValueByColumnAndRow($key + 1, 1, $value);
            }

            $data = $model->search();
            $data->setPagination(false);

            $row = 2;
            foreach ($data->getData() as $val) {
                $rowData = array(
                    $val->idPeriodeAkademik->tahun_akademik,
                    $val->idMataKuliahKurikulum->idKurikulum->nama,
                    $val->idMataKuliahKurikulum->idMataKuliah->kode,
                    $val->idMataKuliahKurikulum->idMataKuliah->nama,
                    $val->pengajarExport,
                    $val->idMataKuliahKurikulum->idMataKuliah->semester,
                    $val->idMataKuliahKurikulum->idMataKuliah->sks_jumlah,
                    $val->kategori,
                );

                foreach ($rowData as $key => $value) {
                    $sheet->setCellValueByColumnAndRow($key + 1, $row, $value);
                    $sheet->getStyleByColumnAndRow($key + 1, $row)->getAlignment()->setWrapText(true);
                }

                $row++;
            }

            $char = ord('B');
            for ($i = 0; $i < 8; $i++) {
                $sheet->getColumnDimension(chr($char++))->setAutoSize(true);
            }

            $writer = new Xlsx($ps);
            ob_end_clean();

            header('Content-type: application/ms-excel');
            header('Content-Disposition: attachment;filename=Data Dosen Matakuliah.xlsx');
            header('Cache-Control: max-age=0');
            $writer->save('php://output');

            exit();
        }

        $this->load->yii_view('layouts/main', [
            'view' => 'kelas/list',
            'params' => [
                'model' => $model,
            ],
            true
        ]);
    }

    public function form_create()
    {
        $idInstitusi    = $this->session->userdata('user_institution_id');
        $model          = new TranKelas();
        $model->id_periode_akademik = $this->session->userdata('user_institution_periode_id');
        $action         = 'kelas/create';
        $this->renderPartial('/kelas/form', ['model' => $model, 'action' => $action, 'idInstitusi' => $idInstitusi], false, true);
    }

    public function create()
    {
        if (isset($_POST['TranKelas'])) {
            $model = new TranKelas();
            $model->attributes = $_POST['TranKelas'];

            $this->modelValidation($model);
            if ($model->save()) {
                echo 'Success';
            } else
                echo CHtml::errorSummary($model, null);
        }
    }

    public function form_update($id)
    {
        $idInstitusi    = $this->session->userdata('user_institution_id');
        $model          = TranKelas::model()->findByPk($id);
        $action         = 'kelas/update/' . $id;
        $this->renderPartial('/kelas/form', ['model' => $model], false, true);
    }

    public function update($id)
    {
        if (isset($_POST['TranKelas'])) {
            $model = TranKelas::model()->findByPk($id);
            if ($model) {
                $model->attributes = $_POST['TranKelas'];
                $this->modelValidation($model);
                if ($model->save()) {
                    echo 'Success';
                } else
                    echo CActiveForm::validate($model);
            } else {
                echo 'Data tidak ditemukan';
            }
        }
    }

    public function delete($id)
    {
        $model  = TranKelas::model()->findByPk($id);
        if ($model) {
            if ($model->delete()) {
                echo 'Success...';
            } else {
                echo 'Failed...';
            }
        }
    }

    public function detail($id)
    {
        $kelas = TranKelas::model()->findByPk($id);
        if ($kelas) {
            $model   = new CArrayDataProvider(
                TranPengajarKelas::model()->with('idKelas')->findAllByAttributes(['id_kelas' => $kelas->id]),
                [
                    'pagination' => false,
                ]
            );

            $this->load->yii_view('layouts/main', [
                'view' => 'kelas/detail',
                'params' => [
                    'kelas' => $kelas,
                    'model' => $model,
                ],
                true
            ]);
        }
    }

    public function get_pengajar($id)
    {
        $model = new CArrayDataProvider(
            TranPenggunaSistem::model()->with(['idRole' => ['alias' => 'tr']])->findAll(['condition' => "tr.nama_singkat = 'dosen'", 'order' => 't.id_institusi']),
            [
                'pagination' => false,
            ]
        );
        $this->renderPartial('/kelas/_chose_pengajar', ['model' => $model, 'idKelas' => $id], false, false);
    }


    public function list_kelas_pengajar($id)
    {
        $pengajar = TranPengajarKelas::model()->findByPk($id);
        if ($pengajar) {
            $model = new TranKelasPengajarKelas('search');
            $model->unsetAttributes();
            $model->id_pengajar_kelas = $id;
            $model = $model->search();


            $this->renderPartial('/kelas/list_kelas_pengajar', ['model' => $model, 'pengajar' => $pengajar], false, false);
        }
    }

    public function form_kelas_pengajar($idPengajar, $idKelasPengajar = null)
    {
        $model = new TranKelasPengajarKelas();

        if ($idKelasPengajar)
            $model = TranKelasPengajarKelas::model()->findByPk($idKelasPengajar);

        $this->renderPartial('/kelas/form_kelas_pengajar', ['model' => $model, 'idPengajar' => $idPengajar], false, false);
    }

    public function save_kelas_pengajar($idPengajar, $idKelasPengajar = null)
    {
        $model = new TranKelasPengajarKelas();
        if ($idKelasPengajar)
            $model = TranKelasPengajarKelas::model()->findByPk($idKelasPengajar);

        $model->id_pengajar_kelas = $idPengajar;
        $model->attributes = $this->input->post('TranKelasPengajarKelas');

        $this->modelValidation($model);
        if ($model->save()) {
            echo 'Success';
        } else
            echo CActiveForm::validate($model);
    }

    public function delete_kelas_pengajar($id)
    {
        $model = TranKelasPengajarKelas::model()->findByPk($id);
        if ($model) {
            $model->delete();
            echo 'success';
        }
    }


    public function set_pengajar()
    {
        if (isset($_POST['id_tran_pengguna_sistem'])) {
            foreach ($this->input->post('id_tran_pengguna_sistem') as $key => $value) {
                $model = new TranPengajarKelas();
                $model->id_kelas    = $this->input->post('id_kelas');
                $model->id_pengajar = $value;

                $this->modelValidation($model);
                if ($model->save()) {
                    echo 'Success';
                } else
                    echo CActiveForm::validate($model);
            }
        }
    }

    public function  delete_pengajar($id)
    {
        $model = TranPengajarKelas::model()->findByPk($id);
        if ($model) {
            $model->delete();
        }
    }
}
