<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    public function index()
    {
        $this->login();
    }
    public function login()
    {
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        if (!isset($isLoggedIn) || $isLoggedIn != TRUE) {
            $this->load->view('page/login');
        } else {
            redirect('/apps');
        }
    }

    public function processLogin()
    {
        $username   = trim($this->input->post('email'));
        $password   = $this->input->post('password');

        $model      = TmstPengguna::model()->findByAttributes(['username' => $username]);

        if (!empty($model)) {
            if (verifyHashedPassword($password, $model->password)) {
                $this->authorize($model);
            } else {
                echo json_encode(array('status' => 'error', 'msg' => 'Password Salah !'));
            }
        } else {
            echo json_encode(array('status' => 'error', 'msg' => 'Login Gagal !'));
        }
    }

    protected function authorize($model, $redirect = false)
    {
        $sessionArray = array(
            'user_id'           => $model->id,
            'user_nama'         => $model->nama,
            'user_last_login'   => $model->last_login,
            'isLoggedIn'        => TRUE
        );

        $modelAuth = TranPenggunaSistem::model()->findByAttributes(['id_pengguna' => $model->id,]);

        if ($modelAuth) {
            $sessionArray['user_role_name']         = $modelAuth->idRole->nama_singkat;
            $sessionArray['user_role_id']           = $modelAuth->idRole;
            $sessionArray['user_divisi_id']         = $modelAuth->id_divisi;
            $sessionArray['user_divisi_name']       = $modelAuth->idDivisi ? $modelAuth->idDivisi->nama : '-';

            // set periode sesion
            // register session
            $this->session->set_userdata($sessionArray);

            $model->last_login =  date('Y-m-d H:i:s');
            if ($model->save(false)) {
                if ($redirect) {
                    redirect('apps');
                } else {
                    echo json_encode(array('status' => 'success', 'msg' => 'Login success'));
                }
            }
        } else {
            echo json_encode(array('status' => 'error', 'msg' => 'Akun Anda belum memiliki role !'));
        }
    }
}
