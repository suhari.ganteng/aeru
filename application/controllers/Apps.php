<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

class Apps extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();
    }

    public function index()
    {
        $this->loadViews("page/dashboard", NULL);
    }

    public function denied()
    {
        $this->loadDenied();
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('apps');
    }

    public function gantiperan()
    {
        if ($this->input->post('id_pengguna_sistem') != null) {
            $modelAuth = TranPenggunaSistem::model()->findByPk($this->input->post('id_pengguna_sistem'));
            if ($modelAuth) {
                $sessionArray['user_role_name']         = $modelAuth->idRole->nama_singkat;
                $sessionArray['user_role_id']           = $modelAuth->idRole;
                $sessionArray['user_institution_id']    = $modelAuth->id_institusi;
                $sessionArray['user_institution_name']  = isset($modelAuth->id_institusi) ? $modelAuth->idInstitusi->nama : '-';

                // set periode sesion
                if (isset($modelAuth->idInstitusi->idPeriodeAkademik) || $modelAuth->idRole->nama_singkat == 'admin') {

                    if ($modelAuth->idRole->nama_singkat != 'admin') {
                        $sessionArray['user_institution_periode_id']                = $modelAuth->idInstitusi->idPeriodeAkademik->id;
                        $sessionArray['user_institution_periode_tahun_akademik']    = $modelAuth->idInstitusi->idPeriodeAkademik->tahun_akademik;
                    } else {
                        $sessionArray['user_institution_periode_id']                = '-';
                        $sessionArray['user_institution_periode_tahun_akademik']    = '-';
                    }

                    $this->session->set_userdata($sessionArray);

                    redirect('apps');
                } else {
                    $this->session->set_flashdata('error', 'Ganti Peran Gagal ! (Tidak Ada Periode Akademik Aktif)');
                }
            }
        }
        $this->loadViews("page/ganti_peran", NULL, NULL, NULL);
    }

    public function akun()
    {
        $id     = $this->session->userdata('user_id');
        $model  = TmstPengguna::model()->findByPk($id);

        if (isset($_POST['TmstPengguna'])) {
            $model->attributes = $this->input->post('TmstPengguna');

            if ($this->input->post('password')) {
                $model->password = getHashedPassword($this->input->post('password'));
            }

            if ($model->save()) {
                echo 'Data berhasil disimpan';
            } else {
                echo CHtml::errorSummary($model, '');
            }

            exit;
        }

        $this->load->yii_view('layouts/main', [
            'view' => 'page/akun',
            'params' => [
                'model' => $model,
            ],
            true
        ]);
    }

    public function photo()
    {
        $id     = $this->session->userdata('user_id');
        $photo  = $this->input->post('photo');

        list($type, $data)  = explode(';', $photo);
        list(, $data)       = explode(',', $photo);
        $data               = base64_decode($data);
        $random             = time();
        $photo_name         = $random . '.png';

        file_put_contents(ASSETPATH . 'files/photo/' . $photo_name, $data);

        $model  = TmstPengguna::model()->findByPk($id);
        @unlink(ASSETPATH . 'files/photo/' . $model->photo);

        $model->photo = $photo_name;
        echo $model->save() ? json_encode(['status' => 'success', 'msg' => 'Data berhasil disimpan']) : json_encode(['status' => 'error', 'msg' => CHtml::errorSummary($model, '')]);
    }

    public function konfigurasi()
    {
        $id     = $this->session->userdata('user_institution_id');
        $model  = TmstInstitusi::model()->findByPk($id);

        if (isset($_POST['TmstInstitusi'])) {
            $model->attributes = $this->input->post('TmstInstitusi');

            if ($model->save()) {
                echo 'Data berhasil disimpan';
            } else {
                echo CHtml::errorSummary($model, '');
            }

            exit;
        }

        $this->load->yii_view('layouts/main', [
            'view' => 'page/konfigurasi',
            'params' => [
                'model' => $model,
            ],
            true
        ]);
    }

    public function testEmail()
    {
        $data['message'] = 'hallo, iam ucen';
        // $data['message'] = 'hallo, iam ucen';
        $this->sendEmail($data);
    }
}
