<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Nota extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();
        if (!in_array($this->session->userdata('user_role_name'), ['admin'])) {
            redirect('apps/denied');
        }
    }

    public function index()
    {
        $model = new TmstNota('search');
        $model->unsetAttributes();

        if (isset($_GET['TmstNota']))
            $model->attributes = $_GET['TmstNota'];

        $this->load->yii_view('layouts/main', [
            'view' => 'admin/nota_index',
            'params' => [
                'model' => $model,
            ],
            true
        ]);
    }

    public function concern()
    {
        $model = new TmstNota('search');
        $model->unsetAttributes();

        if (isset($_GET['TmstNota']))
            $model->attributes = $_GET['TmstNota'];

        $this->load->yii_view('layouts/main', [
            'view' => 'admin/concern_index',
            'params' => [
                'model' => $model,
            ],
            true
        ]);
    }

    public function form_concern()
    {
        $model = TmstNota::model();

        $this->load->yii_view('layouts/main', [
            'view' => 'admin/add_concern',
            'params' => [
                'model' => $model,
            ],
            true
        ]);
    }

    public function nota_form()
    {
        $model = TmstNota::model();

        $this->load->yii_view('layouts/main', [
            'view' => 'admin/nota_form',
            'params' => [
                'model' => $model,
            ],
            true
        ]);
    }

    public function nota_save()
    {
        $model = new TmstNota();
        $model->setScenario('insertU');

        $old_file           = $model->file_nota;
        $model->attributes  = $this->input->post('TmstNota');

        $this->modelValidation($model);
        if ($model->save()) {
            $model->file_nota   = CUploadedFile::getInstance($model, 'file_nota');

            if ($model->file_nota) {
                $new_file_name    = time() . '_' . $model->file_nota;
                $model->file_nota->saveAs(ASSETPATH . 'files/nota/' . $new_file_name);
                $model->file_nota   = $new_file_name;
            } else {
                $model->file_nota = $old_file;
            }

            if ($old_file) {
                @unlink(ASSETPATH . 'files/nota/' . $old_file);
            }
            $model->save(false);

            $file_pendukungs = CUploadedFile::getInstancesByName('file_pendukung');
            if (isset($file_pendukungs) && count($file_pendukungs) > 0) {
                foreach ($file_pendukungs  as $key => $value) {
                    if ($value->saveAs(ASSETPATH . 'files/pendukung/' . time() . '_'  . $value->name)) {
                        $img_add = new TranDataPendukung();
                        $img_add->file_pendukung = time() . '_'  . $value->name;
                        $img_add->id_nota = $model->id;
                        $img_add->save(false);
                    }
                }
            }

            echo 'Data berhasil disimpan';
        } else {
            echo CHtml::errorSummary($model);
        }
    }

    public function add_concern()
    {
        $model  = TmstNota::model();
        if (isset($_POST['TmstNota'])) {
            $model->attributes = $this->input->post('TmstNota');
            if ($model->save()) {
                echo 'Data berhasil disimpan';
            } else {
                echo CHtml::errorSummary($model, '');
            }
            exit;
        }
    }

    public function mon_save($id)
    {
        $model      = TmstNota::model()->findByPk($id);
        $model->attributes  = $this->input->post('TmstNota');


        if (isset($_POST['TmstNota'])) {
            $model->attributes = $this->input->post('TmstNota');
            if ($model->save()) {
                echo 'Data berhasil disimpan';
            } else {
                echo CHtml::errorSummary($model, '');
            }
            exit;
        }
    }

    public function review()
    {
        $model = new TmstNota('search');
        $model->unsetAttributes();

        if (isset($_GET['TmstNota']))
            $model->attributes = $_GET['TmstNota'];

        $this->load->yii_view('layouts/main', [
            'view' => 'admin/monitoring_risiko',
            'params' => [
                'model' => $model,
            ],
            true
        ]);
    }

    public function addmo_form($id)
    {
        $model = TmstNota::model()->findByPk($id);
        if ($model) {
            $this->renderPartial('/admin/add_monitoring', ['model' => $model], false, true);
        } 
    }

    public function form_review($id)
    {
        $model  = TmstNota::model()->findByPk($id);
        if ($model) {
            $this->renderPartial('/nota/form_review', ['model' => $model], false, true);
        }
    }

    public function nota_edit($id)
    {
        $model = TmstNota::model()->findByPk($id);
        $modelPendukung = TranDataPendukung::model()->findAllByAttributes(['id_nota' => $id]);
        $this->renderPartial('/admin/nota_form_edit', ['model' => $model, 'modelPendukung' => $modelPendukung], false, true);
    }

    public function concern_edit($id)
    {
        $model = TmstNota::model()->findByPk($id);
        $this->renderPartial('/admin/edit_concern', ['model' => $model], false, true);
    }

    public function concern_update($id)
    {
        $model      = TmstNota::model()->findByPk($id);
        $model->attributes  = $this->input->post('TmstNota');

        $this->modelValidation($model);
        if ($model->save()) {
            echo 'success';
        } else {
            echo CHtml::errorSummary($model);
        }
    }

    public function nota_update($id)
    {
        $model      = TmstNota::model()->findByPk($id);
        $old_file   = $model->file_nota;

        $model->attributes  = $this->input->post('TmstNota');

        $this->modelValidation($model);
        if ($model->save()) {
            $model->file_nota   = CUploadedFile::getInstance($model, 'file_nota');

            if ($model->file_nota) {
                $new_file_name      = time() . '_' . $model->file_nota;
                $model->file_nota->saveAs(ASSETPATH . 'files/nota/' . $new_file_name);
                $model->file_nota   = $new_file_name;
            } else {
                $model->file_nota = $old_file;
            }

            if ($old_file) {
                @unlink(ASSETPATH . 'files/nota/' . $old_file);
            }

            $model->save(false);

            $file_pendukungs = CUploadedFile::getInstancesByName('file_pendukung');
            if (isset($file_pendukungs) && count($file_pendukungs) > 0) {
                foreach ($file_pendukungs  as $key => $value) {
                    if ($value->saveAs(ASSETPATH . 'files/pendukung/' . time() . '_'  . $value->name)) {
                        $img_add = new TranDataPendukung();
                        $img_add->file_pendukung = time() . '_'  . $value->name;
                        $img_add->id_nota = $model->id;
                        $img_add->save(false);
                    }
                }
            }
            echo 'Success';
        } else {
            echo CHtml::errorSummary($model);
        }
    }

    public function delete($id)
    {
        TranDataPendukung::model()->deleteAllByAttributes(['id_nota' => $id]);
        $model = TmstNota::model()->findByPk($id);
        if ($model) {
            $model->delete();
            echo 'success';
        }
    }

    public function export($id)
    {
        $ps    = new Spreadsheet;
        $sheet = $ps->getActiveSheet();

        $header = array(
            'Nomor NJK',
            'Nomor Review',
            'Tanggal Review',
            'Judul Nota',
            'Divisi',
            'concern',
            'Tindak Lanjut',
            'Target Waktu',
            'Keterangan',
        );

        foreach ($header as $key => $value) {
            $sheet->setCellValueByColumnAndRow($key + 1, 1, $value);
        }
        // $data = $model->search(); //ieu munkabeh row
        // $data->setPagination(false);
        $model = TmstNota::model()->findByPk($id); //kalo findall baru foreach. tinggal beneran eta kolom na, ieu id nota lain id concern, 

        $row = 2;
        // print_r($model);die; //wewh dataan?
        // foreach ($model as $val) { per row ga pake foreach
        $rowData = array(
            $model->tgl_nota,
            $model->no_review,
            $model->tanggal_diperiksa,
            $model->judul,
            $model->id_divisi,
           strip_tags($model->concern),
           strip_tags($model->tindak_lanjut),
           strip_tags($model->target_waktu),
           strip_tags($model->keterangan),
        );
        //alt + shift + f  = formatter php 
        foreach ($rowData as $key => $value) {
            $sheet->setCellValueByColumnAndRow($key + 1, $row, $value);
            $sheet->getStyleByColumnAndRow($key + 1, $row)->getAlignment()->setWrapText(true);
        }

        $row++;
        // }

        $char = ord('B');
        for ($i = 0; $i < 8; $i++) {
            $sheet->getColumnDimension(chr($char++))->setWidth(40);
        }

        $writer = new Xlsx($ps);
        ob_end_clean();

        header('Content-type: application/ms-excel');
        header('Content-Disposition: attachment;filename=Data Risiko.xlsx');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');

        exit();
    }

}
