<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Pengusul extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        if (!in_array($this->session->userdata('user_role_name'), ['pengusul'])) {
            redirect('apps/denied');
        }
    }

    public function nota()
    {
        $model = new TmstNota('search');
        $model->unsetAttributes();

        if (isset($_GET['TmstNota']))
            $model->attributes = $_GET['TmstNota'];

            $model->id_divisi = $this->session->userdata('user_divisi_id');
            $this->load->yii_view('layouts/main', [
            'view' => 'pengusul/nota_index',
            'params' => [
                'model' => $model,
            ],
            true
        ]);
    }

    public function nota_edit($id)
    {
        $model = TmstNota::model()->findByPk($id);
        $modelPendukung = TranDataPendukung::model()->findAllByAttributes(['id_nota' => $model->id]);
        $this->load->yii_view('_layouts/main', [
            'view' => 'admin/nota_form_edit',
            'params' => [
                'model' => $model,
                'modelPendukung' => $modelPendukung,
            ],
            true
        ]);
    }

    public function concern_update($id)
    {
        $model      = TmstNota::model()->findByPk($id);
        $model->attributes  = $this->input->post('TmstNota');

        $this->modelValidation($model);
        if ($model->save()) {
            echo 'success';
        } else {
            echo CHtml::errorSummary($model);
        }
    }

    public function concern()
    {
        $model = new TmstNota('search');
        $model->unsetAttributes();

        if (isset($_GET['TmstNota']))
            $model->attributes = $_GET['TmstNota'];

            $model->id_divisi = $this->session->userdata('user_divisi_id');
            $this->load->yii_view('layouts/main', [
            'view' => 'pengusul/concern_index',
            'params' => [
                'model' => $model,
            ],
            true
        ]);
    }

    public function mon_save($id)
    {
        $model      = TmstNota::model()->findByPk($id);
        $model->attributes  = $this->input->post('TmstNota');

            if (isset($_POST['TmstNota'])){
                $model->attributes = $this->input->post('TmstNota');
                if ($model->save()) {
                    echo 'Data berhasil disimpan';
                } else {
                    echo CHtml::errorSummary($model, '');
                }
                exit;
            }
    }
    
    public function review()
    {
        $model = new TmstNota('search');
        $model->unsetAttributes();

        if (isset($_GET['TmstNota']))
            $model->attributes = $_GET['TmstNota'];

        $model->id_divisi = $this->session->userdata('user_divisi_id');
        $this->load->yii_view('layouts/main', [
            'view' => 'pengusul/monitoring_risiko',
            'params' => [
                'model' => $model,
            ],
            true
        ]);
    }
    public function addmo_form($id)
    {
        $model = TmstNota::model()->findByPk($id);
        $this->renderPartial('/pengusul/add_monitoring', ['model' => $model], false, true);
    }
    public function form_concern()
    {
        $model = TmstNota::model();
        $this->load->yii_view('layouts/main', [
            'view' => 'pengusul/add_concern',
            'params' => [
                'model' => $model,
            ],
            true
        ]);
    }

    public function add_concern()
    {
        $model  = TmstNota::model();
        if (isset($_POST['TmstNota'])){
            $model->attributes = $this->input->post('TmstNota');
            if ($model->save()) {
                echo 'Data berhasil disimpan';
            } else {
                echo CHtml::errorSummary($model, '');
            }
            exit;
        }
    }

    public function concern_edit($id)
    {
        $model = TmstNota::model()->findByPk($id);
        $this->renderPartial('/pengusul/edit_concern', ['model' => $model], false, true);
    }

    public function export($id)
    {
        $ps    = new Spreadsheet;
        $sheet = $ps->getActiveSheet();

        $header = array(
            'Nomor NJK',
            'Nomor Review',
            'Tanggal Review',
            'Judul Nota',
            'Divisi',
            'concern',
            'Tindak Lanjut',
            'Target Waktu',
            'Keterangan',
        );

        foreach ($header as $key => $value) {
            $sheet->setCellValueByColumnAndRow($key + 1, 1, $value);
        }
        // $data = $model->search(); //ieu munkabeh row
        // $data->setPagination(false);
        $model = TmstNota::model()->findByPk($id); //kalo findall baru foreach. tinggal beneran eta kolom na, ieu id nota lain id concern, 

        $row = 2;
        // print_r($model);die; //wewh dataan?
        // foreach ($model as $val) { per row ga pake foreach
        $rowData = array(
            $model->tgl_nota,
            $model->no_review,
            $model->tanggal_diperiksa,
            $model->judul,
            $model->id_divisi,
           strip_tags($model->concern),
           strip_tags($model->tindak_lanjut),
           strip_tags($model->target_waktu),
           strip_tags($model->keterangan),
        );
        //alt + shift + f  = formatter php 
        foreach ($rowData as $key => $value) {
            $sheet->setCellValueByColumnAndRow($key + 1, $row, $value);
            $sheet->getStyleByColumnAndRow($key + 1, $row)->getAlignment()->setWrapText(true);
        }

        $row++;
        // }

        $char = ord('B');
        for ($i = 0; $i < 8; $i++) {
            $sheet->getColumnDimension(chr($char++))->setWidth(40);
        }

        $writer = new Xlsx($ps);
        ob_end_clean();

        header('Content-type: application/ms-excel');
        header('Content-Disposition: attachment;filename=Data Risiko.xlsx');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');

        exit();
    }

    // public function concern_risiko()
    // {
    //     $crud = $this->generate_crud('tmst_concern', 'Concern Risiko ');
    //     $crud->set_subject('Tindak Lanjut Concern Risiko');

    //     //$crud->columns('no_njk', 'nomor_review', 'tanggal_review', 'judul_nota', 'concern', 'tindak_lanjut', 'target_waktu', 'keterangan');
    //     $crud->columns('no_njk', 'nomor_review', 'tanggal_review', 'judul_nota');
    //     $crud->display_as('no_njk','No NJK'); $crud->display_as('nomor_review','No Review') ;$crud->display_as('tanggal_review','Tgl Review'); $crud->display_as('judul_nota','Judul Nota');
    //    // $crud->display_as('concern','Concern') ;$crud->display_as('tindak_lanjut','Tindak Lanjut'); $crud->display_as('target_waktu','Target Waktu'); $crud->display_as('keterangan','Keterangan');
    //     $crud->set_read_fields('concern', 'tindak_lanjut');
    //     $crud->unset_Fields('concern');

    //     $crud->edit_fields('concern', 'tindak_lanjut', 'keterangan');
     
    //     //$crud->unset_edit();
    //     //$crud->unset_read();
    //     $crud->unset_delete();
    //     $crud->unset_add();
    //     $crud->unset_export();
    //     $crud->unset_print();

    //     $output = $crud->render();
    //     $this->loadViews("crud", (array)$output);
    // }
}
