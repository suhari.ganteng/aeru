<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

class Master extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();
        if (!in_array($this->session->userdata('user_role_name'), ['admin'])) {
            redirect('apps/denied');
        }
    }


    public function divisi()
    {
        $crud = $this->generate_crud('tref_divisi', 'Divisi');
        $crud->unset_export();
        $crud->unset_print();
        $crud->unset_read();
        $output = $crud->render();
        $this->loadViews("crud", (array)$output);
    }

    public function pengguna()
    {
        $crud = $this->generate_crud('tmst_pengguna', 'Pengguna');

        $crud->columns('np', 'nama', 'username', 'last_login');

        if ($this->session->userdata('user_role_name') == 'admin') {
            $crud->fields('nama', 'np', 'username', 'password');
        } else {
            $crud->fields('nama', 'username');
        }


        $crud->callback_before_insert(array($this, 'callback_before_insert_pengguna'));
        $crud->callback_before_update(array($this, 'callback_before_update_pengguna'));

        if ($crud->getState() == "edit") {
            $crud->callback_field('password', array($this, 'password_callback'));
        }

        if ($crud->getState() == "update_validation" || $crud->getState() == "update") {
            $crud->columns('np', 'nama', 'username');
        } else {
            $crud->columns('np', 'nama', 'username', 'last_login');
        }

        if ($this->session->userdata('user_role_name') != 'admin') {
            // $crud->unset_delete();
        }

        // $crud->unset_back_to_list();
        $crud->unset_read();
        $crud->unset_export();
        $crud->unset_print();
        $output = $crud->render();
        $this->loadViews("crud", (array)$output);
    }

    public function password_callback($name, $value)
    {
        return "<input id='field-password' type='password' name='password' value='' class='form-control input-sm' autocomplete='new-password'/>
        <small class='text-muted'>Biarkan kosong jika password tidak di ganti</small>
        <script>$(document).ready(function() { $('#field-password').val('') });</script>
        ";
    }

    public function callback_before_insert_pengguna($post_array, $primary_key = null)
    {
        $post_array['password'] = getHashedPassword($post_array['password']);
        return $post_array;
    }

    public function callback_before_update_pengguna($post_array, $primary_key = null)
    {
        if ($post_array['password'] == null) {
            unset($post_array['password']);
        } else {
            $post_array['password'] = getHashedPassword($post_array['password']);
        }
        return $post_array;
    }

    public function pengguna_sistem()
    {
        $crud = $this->generate_crud('tran_pengguna_sistem', 'Pengguna Sistem');

        $crud->display_as('id_pengguna', 'Pengguna');
        $crud->display_as('id_role', 'Role');
        $crud->display_as('id_divisi', 'Pengusul');

        $crud->set_relation('id_pengguna', 'tmst_pengguna', '{nama}');
        $crud->set_relation('id_divisi', 'tref_divisi', '{nama}');

        if ($this->session->userdata('user_role_name') == 'admin') {
            $crud->set_relation('id_role', 'tref_role', '{nama}');
        } else {
            $crud->set_relation('id_role', 'tref_role', '{nama}', "nama_singkat != 'admin'");
        }

        $crud->unset_back_to_list();
        $crud->unset_read();
        $crud->unset_export();
        $crud->unset_print();
        $output = $crud->render();
        $this->loadViews("crud", (array)$output);
    }

}
