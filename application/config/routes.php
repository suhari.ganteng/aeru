<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = "auth";
$route['404_override'] = 'error_404';

