<?php
ini_set('max_execution_time', 0);
ini_set('memory_limit', '12048M');

$root_folder = dirname(__FILE__) . DIRECTORY_SEPARATOR;

define('ROOTPATH', $root_folder);

require ROOTPATH . 'vendor/autoload.php';
$dotenv = Dotenv\Dotenv::create(ROOTPATH);
$dotenv->load();

define('ENVIRONMENT', getenv('ENVIRONMENT'));

switch (ENVIRONMENT) {
	case 'dev':
		error_reporting(-1);
		ini_set('display_errors', 1);
		break;

	case 'testing':
	case 'prod':
		ini_set('display_errors', 0);
		if (version_compare(PHP_VERSION, '5.3', '>=')) {
			error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
		} else {
			error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_USER_NOTICE);
		}
		break;

	default:
		header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
		echo 'The application environment is not set correctly.';
		exit(1); // EXIT_ERROR
}

$system_path 		= dirname(__FILE__) . DIRECTORY_SEPARATOR . 'system';
$application_folder = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'application';
$view_folder 		= dirname(__FILE__) . DIRECTORY_SEPARATOR . 'application/views';
$asset_folder 		= dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets/';



// Set the current directory correctly for CLI requests
if (defined('STDIN')) {
	chdir(dirname(__FILE__));
}

if (($_temp = realpath($system_path)) !== FALSE) {
	$system_path = $_temp . DIRECTORY_SEPARATOR;
} else {
	// Ensure there's a trailing slash
	$system_path = strtr(
		rtrim($system_path, '/\\'),
		'/\\',
		DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR
	) . DIRECTORY_SEPARATOR;
}

// Is the system path correct?
if (!is_dir($system_path)) {
	header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
	echo 'Your system folder path does not appear to be set correctly. Please open the following file and correct this: ' . pathinfo(__FILE__, PATHINFO_BASENAME);
	exit(3); // EXIT_CONFIG
}

// The name of THIS file
define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

// Path to the system directory
define('BASEPATH', $system_path);
define('ASSETPATH', $asset_folder);

// Path to the front controller (this file) directory
define('FCPATH', dirname(__FILE__) . DIRECTORY_SEPARATOR);

// Name of the "system" directory
define('SYSDIR', basename(BASEPATH));


// The path to the "application" directory
if (is_dir($application_folder)) {
	if (($_temp = realpath($application_folder)) !== FALSE) {
		$application_folder = $_temp;
	} else {
		$application_folder = strtr(
			rtrim($application_folder, '/\\'),
			'/\\',
			DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR
		);
	}
} elseif (is_dir(BASEPATH . $application_folder . DIRECTORY_SEPARATOR)) {
	$application_folder = BASEPATH . strtr(
		trim($application_folder, '/\\'),
		'/\\',
		DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR
	);
} else {
	header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
	echo 'Your application folder path does not appear to be set correctly. Please open the following file and correct this: ' . SELF;
	exit(3); // EXIT_CONFIG
}

define('APPPATH', $application_folder . DIRECTORY_SEPARATOR);

// The path to the "views" directory
if (!isset($view_folder[0]) && is_dir(APPPATH . 'views' . DIRECTORY_SEPARATOR)) {
	$view_folder = APPPATH . 'views';
} elseif (is_dir($view_folder)) {
	if (($_temp = realpath($view_folder)) !== FALSE) {
		$view_folder = $_temp;
	} else {
		$view_folder = strtr(
			rtrim($view_folder, '/\\'),
			'/\\',
			DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR
		);
	}
} elseif (is_dir(APPPATH . $view_folder . DIRECTORY_SEPARATOR)) {
	$view_folder = APPPATH . strtr(
		trim($view_folder, '/\\'),
		'/\\',
		DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR
	);
} else {
	header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
	echo 'Your view folder path does not appear to be set correctly. Please open the following file and correct this: ' . SELF;
	exit(3); // EXIT_CONFIG
}

define('VIEWPATH', $view_folder . DIRECTORY_SEPARATOR);

/*
 * --------------------------------------------------------------------
 * LOAD THE BOOTSTRAP FILE
 * --------------------------------------------------------------------
 *
 * And away we go...
 */
defined('YII_DEBUG') or define('YII_DEBUG', false); //set true for enable logging , 
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 2);
require_once APPPATH . '/classes/autoloader.php';
require_once APPPATH . '/classes/yii/yii.php';


$base  = ($_SERVER['SERVER_PORT'] == 443 ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'];
$base .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);

$yii_app = new CWebApplication([
	'basePath' => APPPATH,
	'components' => [
		'db' => [
			'connectionString' => 'mysql:host=' . getenv('DB_HOST') . ';dbname=' . getenv('DB_NAME'),
			'emulatePrepare' => true,
			'username' => getenv('DB_USER'),
			'password' => getenv('DB_PASS'),
			'charset' => 'utf8',
			'enableParamLogging' => true,
		],
		'urlManager' => [
			'class' => 'CIUrlManager',
			'urlFormat' => 'path',
			'showScriptName' => false,
			'rules' => [
				'<controller:\w+>/<id:\d+>' => '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
			],
		],
		'assetManager' => [
			'basePath' => __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'public/yii',
			'baseUrl' => $base . 'assets/public/yii',
		],
		'clientScript' => array(
			'scriptMap' => array(
				'jquery.js'     	=> false,
				'jquery.min.js' 	=> false,
				'jquery.ba-bbq.js' 	=> false,
				'jquery-ui.min.js'	=> false,
				'jquery-ui.css'		=> false
			),
		),
		// 'log' => array(
		// 	'class' => 'CLogRouter',
		// 	'routes' => array(
		// 		array(
		// 			'class' => 'CFileLogRoute',
		// 		),
		// 		array(
		// 			'class' => 'UWebLogRoute',
		// 		),
		// 	),
		// ),
	],
]);

$yii_app->controller = new CIWebController('dummy');
// Yii::setPathOfAlias('bootstrap', APPPATH . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'booster');

require_once BASEPATH . 'core/CodeIgniter.php';
